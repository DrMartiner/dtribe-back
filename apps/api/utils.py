from importlib import import_module

from django.conf import settings
from django.contrib import auth
from django.contrib.sessions.models import Session
from rest_framework.generics import get_object_or_404

from apps.users.models import User

SessionStore = import_module(settings.SESSION_ENGINE).SessionStore

DEFAULT_BACKEND_SESSION_KEY = 'django.contrib.auth.backends.ModelBackend'


def get_session(user: User, backend_session_key=None) -> SessionStore:
    session = SessionStore(None)
    session.clear()
    session.cycle_key()

    session[auth.SESSION_KEY] = user._meta.pk.value_to_string(user)
    session[auth.HASH_SESSION_KEY] = user.get_session_auth_hash()
    if backend_session_key:
        session[auth.BACKEND_SESSION_KEY] = backend_session_key
    else:
        session[auth.BACKEND_SESSION_KEY] = DEFAULT_BACKEND_SESSION_KEY

    session.save()

    return session


def get_user_by_session(sessionid: str) -> User:
    # Get session by sessionid
    session = get_object_or_404(Session, session_key=sessionid)

    # Get user ID from the session
    session_data = session.get_decoded()
    uid = session_data.get('_auth_user_id')

    return get_object_or_404(User, id=uid)
