from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'item', views.ItemModelViewSet, base_name='item')

urlpatterns = router.urls
