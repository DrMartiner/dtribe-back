from django_filters import FilterSet, NumberFilter, CharFilter

from apps.shop.models import Item


class ItemFilter(FilterSet):
    tribe_id = NumberFilter(field_name='tribe__id')
    tribe_slug = CharFilter(field_name='tribe__slug')
    name = CharFilter(field_name='name', lookup_expr='icontains')

    price_min = NumberFilter(field_name='price', lookup_expr='gte', min_value=0)
    price_max = NumberFilter(field_name='price', lookup_expr='lte', min_value=0)

    class Meta:
        model = Item
        fields = ['name', 'tribe_id', 'tribe_slug', 'price_min', 'price_max']
