from rest_framework import serializers

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.shop.models import Item


class ItemListRetrieveSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()

    class Meta:
        model = Item
        fields = ['id', 'name', 'slug', 'image', 'text', 'price', 'currency', 'tribe', 'url', 'created', 'updated']
