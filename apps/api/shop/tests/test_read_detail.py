from django_dynamic_fixture import G

from apps.shop.models import Item
from ...base_test import BaseApiTest
from .. import serializers


class ReadDetailItemTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:shop:item-detail'
    serializer = serializers.ItemListRetrieveSerializer

    def test_un_auth(self):
        instance = G(Item, slug='slug')

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)
