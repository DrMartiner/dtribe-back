from django.urls import reverse
from django_dynamic_fixture import G

from apps.shop.models import Item
from ...base_test import BaseApiTest
from .. import serializers


class ReadListItemTest(BaseApiTest):
    url = reverse('api_v1:shop:item-list')
    serializer = serializers.ItemListRetrieveSerializer

    def test_by_un_auth(self):
        for i in range(3):
            G(Item)
        G(Item, is_active=True)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, Item.objects.filter(is_active=True), response)
