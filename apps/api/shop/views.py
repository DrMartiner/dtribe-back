from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.shop.models import Item
from apps.api.views import BaseExtendViewSet
from . import serializers
from . import filters


class ItemModelViewSet(BaseExtendViewSet, ReadOnlyModelViewSet):
    lookup_field = 'slug'
    queryset = Item.objects.filter(is_active=True)
    filter_class = filters.ItemFilter
    serializer_class_map = {
        'list': serializers.ItemListRetrieveSerializer,
        'retrieve': serializers.ItemListRetrieveSerializer,
    }
