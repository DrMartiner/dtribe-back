from django.contrib.auth import login
from django.utils.translation import ugettext_lazy as _

from rest_framework import viewsets, status
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.users.models import User
from apps.api.views import BaseExtendViewSet
from . import serializers


class UserModelViewSet(BaseExtendViewSet, viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.filter(is_active=True)
    serializer_class_map = {
        'list': serializers.UserListRetrieveSerializer,
        'retrieve': serializers.UserListRetrieveSerializer,
    }


class SelfProfileView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user = request.user
        serializer = serializers.SelfProfileRetrieveSerializer(instance=user)

        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        serializer = serializers.SelfProfileUpdateSerializer(data=request.data, instance=request.user)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = User.objects.get(pk=request.user.pk)
        serializer = serializers.SelfProfileRetrieveSerializer(instance=user)

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)


class ChangePasswordView(APIView):
    permission_classes = [IsAuthenticated]

    messages_map = {
        202: _('Changing password was succeed.'),
        403: _('Old password is wrong.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.ChangePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        is_right = request.user.check_password(serializer.data['password_old'])
        if not is_right:
            raise PermissionDenied

        request.user.set_password(serializer.data['password_new1'])
        request.user.save()

        login(request, request.user, 'django.contrib.auth.backends.ModelBackend')

        return Response(status=status.HTTP_202_ACCEPTED)
