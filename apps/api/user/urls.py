from django.urls import path
from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'', views.UserModelViewSet, base_name='user')

urlpatterns = [
    path('self/', views.SelfProfileView.as_view(), name='self'),
    path('change-password/', views.ChangePasswordView.as_view(), name='change-password'),
]

urlpatterns += router.urls
