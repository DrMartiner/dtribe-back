from django.urls import reverse
from django_dynamic_fixture import G

from apps.users.models import User
from ...base_test import BaseApiTest
from .. import serializers


class ReadListUserTest(BaseApiTest):
    url = reverse('api_v1:user:user-list')
    serializer = serializers.UserListRetrieveSerializer

    def test_by_un_auth(self):
        for i in range(3):
            G(User)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, User.objects.all(), response)
