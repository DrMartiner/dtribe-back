from django_dynamic_fixture import G

from apps.users.models import User
from ...base_test import BaseApiTest
from .. import serializers


class ReadDetailUserTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:user:user-detail'
    serializer = serializers.UserListRetrieveSerializer

    def test_by_un_auth(self):
        instance = G(User)

        url = self._get_retrieve_url(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)
