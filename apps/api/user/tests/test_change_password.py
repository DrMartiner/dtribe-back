from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.utils import get_user_by_session


class ChangePasswordTest(BaseApiTest):
    url = reverse('api_v1:user:change-password')

    def setUp(self):
        super(ChangePasswordTest, self).setUp()

        self.post_data = {
            'password_old': self.password,
            'password_new1': '576890ssavsd^&^',
            'password_new2': '576890ssavsd^&^',
        }

    def test_succeed(self):
        self.client.force_login(self.user)
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()
        is_right = self.user.check_password(self.post_data['password_new1'])
        self.assertTrue(is_right)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.user.pk, user.pk)

    def test_wrong_old(self):
        self.post_data['password_old'] = 'wrong'

        self.client.force_login(self.user)
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus403(response)

        self.reload_user_instance()
        is_right = self.user.check_password(self.post_data['password_new1'])
        self.assertFalse(is_right)

    def test_wrong_new2(self):
        self.post_data['password_new2'] = 'wrong'

        self.client.force_login(self.user)
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus400(response)

        self.reload_user_instance()
        is_right = self.user.check_password(self.post_data['password_new1'])
        self.assertFalse(is_right)

    def test_un_auth(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus403(response)
