from django.urls import reverse
from django_dynamic_fixture import G

from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.users.models import User
from ...base_test import BaseApiTest
from .. import serializers


class UpdateSelfProfile(BaseApiTest):
    url = reverse('api_v1:user:self')

    def setUp(self):
        super(UpdateSelfProfile, self).setUp()

        self.post_data = {
            'username': 'new_user_name',
            'first_name': 'New first name',
            'last_name': 'New last name',
        }

    def test_update(self):
        self.client.force_login(self.user)

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()
        self.assertRetrieveInResponse(serializers.SelfProfileRetrieveSerializer, self.user, response)

        user: User = get_object_or_none(User, **self.post_data)
        self.assertIsNotNone(user)

    def test_un_auth(self):
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)
