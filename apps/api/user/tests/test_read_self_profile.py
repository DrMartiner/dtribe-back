from django.urls import reverse
from django_dynamic_fixture import G

from apps.permission.models import Role
from apps.users.models import User
from ...base_test import BaseApiTest
from .. import serializers


class ReadSelfProfile(BaseApiTest):
    url = reverse('api_v1:user:self')

    def test_read(self):
        for i in range(3):
            G(Role, user=self.user)

        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.SelfProfileRetrieveSerializer, self.user, response)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
