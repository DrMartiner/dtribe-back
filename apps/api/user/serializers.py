from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.permission.models import Role
from apps.users.models import User


class UserListRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', ]


class SelfProfileRetrieveSerializer(serializers.ModelSerializer):
    roles = serializers.SerializerMethodField()

    def get_roles(self, obj: User):
        from apps.api.permission.serializers import RoleListRetrieveSerializer

        queryset = Role.objects.filter(user=obj)
        serializer = RoleListRetrieveSerializer(queryset, many=True)

        return serializer.data

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'phone', 'first_name', 'last_name', 'roles']


class SelfProfileUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']


class ChangePasswordSerializer(serializers.Serializer):
    password_old = serializers.CharField(required=True)
    password_new1 = serializers.CharField(required=True)
    password_new2 = serializers.CharField(required=True)

    def validate_password_new2(self, password_new2: str) -> str:
        if password_new2 != self.initial_data.get('password_new1'):
            raise ValidationError(_('Passwords are not equal.'))

        return password_new2
