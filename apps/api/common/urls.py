from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'category', views.CategoryModelViewSet, base_name='category')

urlpatterns = router.urls
