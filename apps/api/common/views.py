from rest_framework import viewsets

from apps.common.models import Category
from apps.api.views import BaseExtendViewSet
from . import serializers


class CategoryModelViewSet(BaseExtendViewSet, viewsets.ReadOnlyModelViewSet):
    lookup_field = 'slug'
    queryset = Category.objects.all()
    serializer_class_map = {
        'list': serializers.CategoryListRetrieveSerializer,
        'retrieve': serializers.CategoryListRetrieveSerializer,
    }
