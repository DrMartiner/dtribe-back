from django.urls import reverse
from django_dynamic_fixture import G

from apps.common.models import Category
from ...base_test import BaseApiTest
from .. import serializers


class ReadListUserTest(BaseApiTest):
    url = reverse('api_v1:common:category-list')
    serializer = serializers.CategoryListRetrieveSerializer

    def test_by_un_auth(self):
        for i in range(3):
            G(Category)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, Category.objects.all(), response)
