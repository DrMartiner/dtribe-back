from django_dynamic_fixture import G

from apps.common.models import Category
from ...base_test import BaseApiTest
from .. import serializers


class ReadDetailCategoryTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:common:category-detail'
    serializer = serializers.CategoryListRetrieveSerializer

    def test_by_un_auth(self):
        instance = G(Category, slug='slug')

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)
