from rest_framework import serializers

from apps.common.models import Category

_SERIALIZER_FIELDS = ['name', 'slug']


class CategoryListRetrieveSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = _SERIALIZER_FIELDS

        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
