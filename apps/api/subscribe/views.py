from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.common.utils import get_object_or_none
from apps.subscribe import models
from . import serializers
from . import permissions


class SubscribeModelViewSet(viewsets.ModelViewSet):
    http_method_names = ['post', 'get', 'delete']
    permission_classes = [permissions.SubscribePermission]

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return models.SubscribeUser.objects.filter(user=self.request.user)
        else:
            return models.SubscribeSession.objects.filter(session__session_key=self.request.session.session_key)

    def get_serializer_class(self):
        is_authenticated = self.request.user.is_authenticated
        if self.action == 'create':
            if is_authenticated:
                return serializers.SubscribeUserCreateSerializer
            else:
                return serializers.SubscribeSessionCreateSerializer

        elif self.action in ['list', 'retrieve']:
            if is_authenticated:
                return serializers.SubscribeUserListRetrieveSerializer
            else:
                return serializers.SubscribeSessionListRetrieveSerializer

        else:
            raise ValueError(f'Action "{self.action}" is not define.')

    def create(self, request, *args, **kwargs):
        data: dict = request.data.copy()
        if request.user.is_authenticated:
            data.update({'user': request.user.pk})
        else:
            data.update({'session': request.session.session_key})

        serializer_request = self.get_serializer(data=data)
        serializer_request.is_valid(raise_exception=True)
        serializer_request.save()

        if self.request.user.is_authenticated:
            serializer_response = serializers.SubscribeUserListRetrieveSerializer(
                context={'request': request},
                instance=serializer_request.instance
            )
        else:
            serializer_response = serializers.SubscribeSessionListRetrieveSerializer(
                context={'request': request},
                instance=serializer_request.instance
            )

        headers = self.get_success_headers(serializer_response.data)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['post'], url_path='check-exists', url_name='check-exists')
    def check_exists(self, request):
        serializer = serializers.SubscribeCheckSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        params = dict(tribe__id=serializer.data['tribe'])
        if self.request.user.is_authenticated:
            params['user'] = request.user
            subscribe = get_object_or_none(models.SubscribeUser, **params)
        else:
            params['session__session_key'] = self.request.session.session_key
            subscribe = get_object_or_none(models.SubscribeSession, **params)

        data = {'is_exists': subscribe is not None}
        if subscribe:
            data['subscribe'] = subscribe.pk
        else:
            data['subscribe'] = None

        return Response(data)
