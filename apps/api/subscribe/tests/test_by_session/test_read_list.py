from http.cookies import SimpleCookie
from importlib import import_module

from django.conf import settings
from django.db.models import QuerySet
from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class ReadListSubscribeSessionTest(BaseApiTest):
    _ulr_list = reverse('api_v1:subscribe:subscribe-list')
    serializer = serializers.SubscribeSessionListRetrieveSerializer

    def setUp(self):
        super(ReadListSubscribeSessionTest, self).setUp()

        self.session = engine.SessionStore()
        self.session.create()

        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

    def test_read(self):
        for i in range(3):
            tribe = G(Tribe)
            G(models.SubscribeSession)
            G(models.SubscribeSession, tribe=tribe)
            G(models.SubscribeSession, tribe=tribe, session=self.session.session_key)

        response = self.client.get(self._ulr_list)
        self.assertResponseStatus200(response)

        queryset: QuerySet = models.SubscribeSession.objects.filter(session=self.session.session_key)
        self.assertListInResponse(self.serializer, queryset, response)
