from http.cookies import SimpleCookie
from importlib import import_module

from django.conf import settings
from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class CheckExistsSubscribeForSessionTest(BaseApiTest):
    def setUp(self):
        super(CheckExistsSubscribeForSessionTest, self).setUp()

        self.session = engine.SessionStore()
        self.session.create()

        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

        self.url = reverse('api_v1:subscribe:subscribe-check-exists')

        self.tribe = G(Tribe)

    def test_is_exists(self):
        subscribe = G(models.SubscribeSession, tribe=self.tribe, session=self.session.session_key)

        response = self.client.post(self.url, data={'tribe': self.tribe.pk})
        self.assertResponseStatus200(response)

        self.assertIn('is_exists', response.json()['payload'])
        self.assertTrue(response.json()['payload']['is_exists'])

        self.assertIn('subscribe', response.json()['payload'])
        self.assertEquals(response.json()['payload']['subscribe'], subscribe.pk)

    def test_is_not_exists(self):
        response = self.client.post(self.url, data={'tribe': self.tribe.pk})
        self.assertResponseStatus200(response)

        self.assertIn('is_exists', response.json()['payload'])
        self.assertFalse(response.json()['payload']['is_exists'])

        self.assertIn('subscribe', response.json()['payload'])
        self.assertIsNone(response.json()['payload']['subscribe'])
