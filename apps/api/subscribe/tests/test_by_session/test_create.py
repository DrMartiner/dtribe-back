from importlib import import_module

from django.conf import settings
from django.urls import reverse
from django_dynamic_fixture import G
from http.cookies import SimpleCookie

from apps.subscribe import models
from apps.tribe.models import Tribe
from ....base_test import BaseApiTest
from ... import serializers

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class CreateSubscribeSessionTest(BaseApiTest):
    _url_list = reverse('api_v1:subscribe:subscribe-list')

    def setUp(self):
        super(CreateSubscribeSessionTest, self).setUp()

        self.tribe = G(Tribe, slug='slug-name')
        self.post_data = {'tribe': self.tribe.pk}

        self.session = engine.SessionStore()
        self.session.create()
        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

    def test_create(self):
        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus201(response)

        instance: models.SubscribeSession = models.SubscribeSession.objects.last()
        self.assertIsNotNone(instance)
        self.assertEquals(instance.tribe, self.tribe)
        self.assertEquals(instance.session.session_key, response.cookies[settings.SESSION_COOKIE_NAME].value)

        self.assertRetrieveInResponse(serializers.SubscribeSessionListRetrieveSerializer, instance, response)

    def test_double(self):
        G(models.SubscribeSession, session=self.session.session_key, tribe=self.tribe)

        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus400(response)

        count: int = models.SubscribeSession.objects.count()
        self.assertEquals(count, 1)

    def test_cold_start(self):
        self.client.cookies = SimpleCookie({})

        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus201(response)

        count: int = models.SubscribeSession.objects.count()
        self.assertEquals(count, 1)

    def test_cold_start_two(self):
        self.client.cookies = SimpleCookie({})

        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus201(response)

        tribe = G(Tribe)

        response = self.client.post(self._url_list, {'tribe': tribe.pk})
        self.assertResponseStatus201(response)

        count: int = models.SubscribeSession.objects.filter(
            session__session_key=response.cookies[settings.SESSION_COOKIE_NAME].value
        ).count()
        self.assertEquals(count, 2)
