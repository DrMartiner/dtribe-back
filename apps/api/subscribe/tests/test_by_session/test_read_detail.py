from http.cookies import SimpleCookie
from importlib import import_module

from django.conf import settings
from django.db.models import QuerySet
from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class ReadDetailSubscribeSessionTest(BaseApiTest):
    serializer = serializers.SubscribeSessionListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(ReadDetailSubscribeSessionTest, self).setUp()

        self.session = engine.SessionStore()
        self.session.create()

        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

        self.tribe = G(Tribe)

    def test_read(self):
        instance = G(models.SubscribeSession, tribe=self.tribe, session=self.session.session_key)

        url = self._get_retrieve_url(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)

    def test_read_alien(self):
        instance = G(models.SubscribeSession, tribe=self.tribe)

        url = self._get_retrieve_url(instance)
        response = self.client.get(url)
        self.assertResponseStatus404(response)
