from http.cookies import SimpleCookie
from importlib import import_module

from django.conf import settings
from django.db.models import QuerySet
from django.urls import reverse
from django_dynamic_fixture import G

from apps.common.utils import get_object_or_none
from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class UpdateSubscribeSessionTest(BaseApiTest):
    serializer = serializers.SubscribeSessionListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(UpdateSubscribeSessionTest, self).setUp()

        self.session = engine.SessionStore()
        self.session.create()

        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

        self.tribe = G(Tribe)

    def test_update(self):
        tribe_new: Tribe = G(Tribe)
        instance = G(models.SubscribeSession, tribe=self.tribe, session=self.session.session_key)

        url = self._get_retrieve_url(instance)
        response = self.client.patch(url, data={'tribe': tribe_new.pk})
        self.assertResponseStatus405(response)

        instance: models.SubscribeSession = get_object_or_none(models.SubscribeSession, pk=instance.pk)
        self.assertEquals(instance.tribe.pk, self.tribe.pk)
        self.assertEquals(instance.session.session_key, self.session.session_key)

    def test_update_alien(self):
        tribe_new: Tribe = G(Tribe)
        instance = G(models.SubscribeSession, tribe=self.tribe)

        url = self._get_retrieve_url(instance)
        response = self.client.patch(url, data={'tribe': tribe_new.pk})
        self.assertResponseStatus405(response)

        instance: models.SubscribeSession = get_object_or_none(models.SubscribeSession, pk=instance.pk)
        self.assertEquals(instance.tribe.pk, self.tribe.pk)
        self.assertNotEquals(instance.session.session_key, self.session.session_key)
