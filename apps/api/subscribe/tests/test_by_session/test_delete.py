from http.cookies import SimpleCookie
from importlib import import_module

from django.conf import settings
from django_dynamic_fixture import G

from apps.common.utils import get_object_or_none
from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class DeleteSubscribeSessionTest(BaseApiTest):
    serializer = serializers.SubscribeSessionListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(DeleteSubscribeSessionTest, self).setUp()

        self.session = engine.SessionStore()
        self.session.create()

        self.client.cookies = SimpleCookie({'sessionid': self.session.session_key})

        self.tribe = G(Tribe)

    def test_delete(self):
        instance = G(models.SubscribeSession, tribe=self.tribe, session=self.session.session_key)

        url = self._get_retrieve_url(instance)
        response = self.client.delete(url)
        self.assertResponseStatus202(response)

        instance = get_object_or_none(models.SubscribeSession, pk=instance.pk)
        self.assertIsNone(instance)

    def test_delete_alien(self):
        instance = G(models.SubscribeSession, tribe=self.tribe)

        url = self._get_retrieve_url(instance)
        response = self.client.delete(url)
        self.assertResponseStatus404(response)

        instance = get_object_or_none(models.SubscribeSession, pk=instance.pk)
        self.assertIsNotNone(instance)
