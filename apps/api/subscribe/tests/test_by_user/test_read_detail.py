from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers


class ReadDetailSubscribeUserTest(BaseApiTest):
    serializer = serializers.SubscribeUserListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(ReadDetailSubscribeUserTest, self).setUp()

        self.tribe = G(Tribe)

    def test_read(self):
        instance = G(models.SubscribeUser, tribe=self.tribe, user=self.user)

        self.client.force_login(self.user)

        url = self._get_retrieve_url(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response, self.user)

    def test_read_alien(self):
        instance = G(models.SubscribeUser, tribe=self.tribe)

        self.client.force_login(self.user)

        url = self._get_retrieve_url(instance)
        response = self.client.get(url)
        self.assertResponseStatus404(response)
