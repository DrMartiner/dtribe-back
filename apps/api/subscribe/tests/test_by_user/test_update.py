from django_dynamic_fixture import G

from apps.common.utils import get_object_or_none
from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers


class UpdateSubscribeUserTest(BaseApiTest):
    serializer = serializers.SubscribeUserListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(UpdateSubscribeUserTest, self).setUp()

        self.tribe = G(Tribe)

    def test_update(self):
        tribe_new: Tribe = G(Tribe)
        instance = G(models.SubscribeUser, tribe=self.tribe, user=self.user)

        url = self._get_retrieve_url(instance)
        response = self.client.patch(url, data={'tribe': tribe_new.pk})
        self.assertResponseStatus405(response)

        instance: models.SubscribeUser = get_object_or_none(models.SubscribeUser, pk=instance.pk)
        self.assertEquals(instance.user, self.user)
        self.assertEquals(instance.tribe.pk, self.tribe.pk)

    def test_update_alien(self):
        tribe_new: Tribe = G(Tribe)
        instance = G(models.SubscribeUser, tribe=self.tribe)

        url = self._get_retrieve_url(instance)
        response = self.client.patch(url, data={'tribe': tribe_new.pk})
        self.assertResponseStatus405(response)

        instance: models.SubscribeUser = get_object_or_none(models.SubscribeUser, pk=instance.pk)
        self.assertEquals(instance.tribe.pk, self.tribe.pk)
        self.assertNotEquals(instance.user, self.user)
