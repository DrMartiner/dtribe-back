from django.db.models import QuerySet
from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers


class ReadListSubscribeUserTest(BaseApiTest):
    _ulr_list = reverse('api_v1:subscribe:subscribe-list')
    serializer = serializers.SubscribeUserListRetrieveSerializer

    def test_read(self):
        for i in range(3):
            tribe = G(Tribe)
            G(models.SubscribeUser)
            G(models.SubscribeUser, tribe=tribe)
            G(models.SubscribeUser, tribe=tribe, user=self.user)

        self.client.force_login(self.user)

        response = self.client.get(self._ulr_list)
        self.assertResponseStatus200(response)

        queryset: QuerySet = models.SubscribeUser.objects.filter(user=self.user)
        self.assertListInResponse(self.serializer, queryset, response, self.user)
