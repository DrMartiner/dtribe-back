from django_dynamic_fixture import G

from apps.common.utils import get_object_or_none
from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest
from ... import serializers


class DeleteSubscribeUserTest(BaseApiTest):
    serializer = serializers.SubscribeUserListRetrieveSerializer
    _url_retrieve_named = 'api_v1:subscribe:subscribe-detail'

    def setUp(self):
        super(DeleteSubscribeUserTest, self).setUp()

        self.tribe = G(Tribe)

    def test_delete(self):
        instance = G(models.SubscribeUser, tribe=self.tribe, user=self.user)

        self.client.force_login(self.user)

        url = self._get_retrieve_url(instance)
        response = self.client.delete(url)
        self.assertResponseStatus202(response)

        instance = get_object_or_none(models.SubscribeUser, pk=instance.pk)
        self.assertIsNone(instance)

    def test_delete_alien(self):
        instance = G(models.SubscribeUser, tribe=self.tribe)

        url = self._get_retrieve_url(instance)
        response = self.client.delete(url)
        self.assertResponseStatus404(response)

        instance = get_object_or_none(models.SubscribeUser, pk=instance.pk)
        self.assertIsNotNone(instance)
