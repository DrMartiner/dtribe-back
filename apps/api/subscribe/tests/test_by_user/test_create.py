from django.urls import reverse
from django_dynamic_fixture import G

from apps.subscribe import models
from apps.tribe.models import Tribe
from ....base_test import BaseApiTest
from ... import serializers


class CreateSubscribeUserTest(BaseApiTest):
    _url_list = reverse('api_v1:subscribe:subscribe-list')

    def setUp(self):
        super(CreateSubscribeUserTest, self).setUp()

        self.tribe = G(Tribe)
        self.post_data = {'tribe': self.tribe.pk}

    def test_create(self):
        self.client.force_login(self.user)

        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus201(response)

        instance: models.SubscribeUser = models.SubscribeUser.objects.last()
        self.assertIsNotNone(instance)
        self.assertEquals(instance.tribe, self.tribe)
        self.assertEquals(instance.user.pk, self.user.pk)

        self.assertRetrieveInResponse(serializers.SubscribeUserListRetrieveSerializer, instance, response)

    def test_double(self):
        G(models.SubscribeUser, user=self.user, tribe=self.tribe)

        self.client.force_login(self.user)

        response = self.client.post(self._url_list, self.post_data)
        self.assertResponseStatus400(response)

        count: int = models.SubscribeUser.objects.count()
        self.assertEquals(count, 1)
