from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.tribe.models import Tribe
from apps.subscribe import models
from ....base_test import BaseApiTest


class CheckExistsSubscribeForUserTest(BaseApiTest):
    def setUp(self):
        super(CheckExistsSubscribeForUserTest, self).setUp()

        self.url = reverse('api_v1:subscribe:subscribe-check-exists')

        self.tribe = G(Tribe)

        self.client.force_login(self.user)

    def test_is_exists(self):
        subscribe = G(models.SubscribeUser, tribe=self.tribe, user=self.user)

        response = self.client.post(self.url, data={'tribe': self.tribe.pk})
        self.assertResponseStatus200(response)

        self.assertIn('is_exists', response.json()['payload'])
        self.assertTrue(response.json()['payload']['is_exists'])

        self.assertIn('subscribe', response.json()['payload'])
        self.assertEquals(response.json()['payload']['subscribe'], subscribe.pk)

    def test_is_not_exists(self):
        response = self.client.post(self.url, data={'tribe': self.tribe.pk})
        self.assertResponseStatus200(response)

        self.assertIn('is_exists', response.json()['payload'])
        self.assertFalse(response.json()['payload']['is_exists'])

        self.assertIn('subscribe', response.json()['payload'])
        self.assertIsNone(response.json()['payload']['subscribe'])
