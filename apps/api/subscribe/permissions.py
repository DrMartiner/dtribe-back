from rest_framework.permissions import BasePermission


class SubscribePermission(BasePermission):
    def has_object_permission(self, request, view, obj) -> bool:
        if request.user.is_authenticated:
            return obj.user.pk == request.user.pk
        else:
            return obj.session.session_key == request.session.session_key
