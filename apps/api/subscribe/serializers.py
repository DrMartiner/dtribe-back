from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.subscribe import models
from apps.tribe.models import Tribe


class SubscribeUserCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.SubscribeUser
        fields = ['id', 'tribe', 'user']
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('user', 'tribe'),
                message=_('Subscribe to the tribe is already exists.')
            )
        ]


class SubscribeUserListRetrieveSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    tribe = TribeListRetrieveSerializer()

    class Meta:
        model = models.SubscribeUser
        fields = ['id', 'tribe']


class SubscribeSessionCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)

    class Meta:
        model = models.SubscribeSession
        fields = ['id', 'tribe', 'session']
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('session', 'tribe'),
                message=_('Subscribe to the tribe is already exists.')
            )
        ]


class SubscribeSessionListRetrieveSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()

    class Meta:
        model = models.SubscribeSession
        fields = ['id', 'tribe']


class SubscribeCheckSerializer(serializers.Serializer):
    tribe = serializers.PrimaryKeyRelatedField(queryset=Tribe.objects.all())

    class Meta:
        fields = ['tribe']
