from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'', views.SubscribeModelViewSet, base_name='subscribe')

urlpatterns = router.urls
