from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'role', views.RoleModelViewSet, base_name='role')

urlpatterns = router.urls
