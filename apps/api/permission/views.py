from django.utils.translation import gettext_lazy as _

from rest_framework.viewsets import ModelViewSet

from apps.permission import models
from apps.api.views import BaseExtendViewSet
from . import serializers
from . import filters
from . import permissions


class RoleModelViewSet(BaseExtendViewSet, ModelViewSet):
    messages_map = {
        201: _('Role was created.'),
        403: _('You do not have permission to perform this action.'),
        409: _('You can\'t add a role for self.'),
    }

    serializer_class_map = {
        'create': serializers.RoleCreateSerializer,
        'list': serializers.RoleListRetrieveSerializer,
        'retrieve': serializers.RoleListRetrieveSerializer,
        'update': serializers.RoleUpdateSerializer,
        'partial_update': serializers.RoleUpdateSerializer,
    }
    filter_class = filters.RoleFilter
    permission_classes = [permissions.RolePermission]

    def get_queryset(self):
        return models.Role.objects.filter(tribe__user__pk=self.request.user.pk)
