from django_filters import FilterSet, NumberFilter, CharFilter

from apps.permission import models


class RoleFilter(FilterSet):
    role = CharFilter(field_name='role', lookup_expr='exact')
    user_id = NumberFilter(field_name='tribe__pk')
    tribe_id = NumberFilter(field_name='tribe__id')
    tribe_slug = CharFilter(field_name='tribe__slug')

    class Meta:
        model = models.Role
        fields = ['role', 'user_id', 'tribe_id', 'tribe_slug']
