from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.users.models import User


class UpdateTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:permission:role-detail'

    def setUp(self):
        super(UpdateTest, self).setUp()

        self.tribe: Tribe = G(Tribe, user=self.user)
        self.moder = G(User)
        self.role = G(
            Role,
            user=self.moder,
            tribe=self.tribe,
            role=Role.ROLE.EVENT_MODER
        )

        self.client.force_login(self.user)

        self.url = self._get_retrieve_url(self.role)
        self.post_data = {'role': Role.ROLE.EVENT_WRITER}

    def test_success(self):
        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus200(response)

        role = get_object_or_none(Role, user=self.moder, tribe=self.tribe,  **self.post_data)
        self.assertIsNotNone(role)

    def test_update_user(self):
        self.post_data['user'] = G(User).pk

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus200(response)

        del self.post_data['user']
        role = get_object_or_none(Role, user=self.moder, tribe=self.tribe,  **self.post_data)
        self.assertIsNotNone(role)

    def test_update_tribe(self):
        self.post_data['tribe'] = G(Tribe, user=self.user).pk

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus200(response)

        del self.post_data['tribe']
        role = get_object_or_none(Role, user=self.moder, tribe=self.tribe,  **self.post_data)
        self.assertIsNotNone(role)

    def test_alien_tribe(self):
        role = G(Role)
        url = self._get_retrieve_url(role)

        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus404(response)

        role = get_object_or_none(Role, user=self.moder, tribe=self.tribe,  **self.post_data)
        self.assertIsNone(role)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus403(response)

        role = get_object_or_none(Role, user=self.moder, tribe=self.tribe, **self.post_data)
        self.assertIsNone(role)
