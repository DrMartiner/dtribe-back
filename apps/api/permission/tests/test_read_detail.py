from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.permission.serializers import RoleListRetrieveSerializer
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.users.models import User


class ReadDetailTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:permission:role-detail'

    def setUp(self):
        super(ReadDetailTest, self).setUp()

        self.tribe: Tribe = G(Tribe, user=self.user)
        self.role = G(Role, tribe=self.tribe)

        self.url = self._get_retrieve_url(self.role)

    def test_success(self):
        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(RoleListRetrieveSerializer, self.role, response, self.user)

    def test_alien(self):
        self.client.force_login(self.user)

        role = G(Role)

        url = self._get_retrieve_url(role)
        response = self.client.get(url)
        self.assertResponseStatus404(response)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
