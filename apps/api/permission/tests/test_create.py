from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.users.models import User


class CreateTest(BaseApiTest):
    url = reverse('api_v1:permission:role-list')

    def setUp(self):
        super(CreateTest, self).setUp()

        self.tribe: Tribe = G(Tribe, user=self.user)
        self.moder = G(User)

        self.post_data = {
            'role': Role.ROLE.EVENT_MODER,
            'user': self.moder.pk,
            'tribe': self.tribe.slug,
        }

        self.client.force_login(self.user)

    def test_success(self):
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus201(response)

        role = get_object_or_none(Role, **self.post_data)
        self.assertIsNotNone(role)

    def test_double(self):
        Role.objects.create(
            role=self.post_data['role'],
            user_id=self.post_data['user'],
            tribe=Tribe.objects.get(slug=self.post_data['tribe']),
        )
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus400(response)

        count = Role.objects.filter(**self.post_data).count()
        self.assertEquals(count, 1)

    def test_alien_tribe(self):
        tribe: Tribe = G(Tribe)
        self.post_data['tribe'] = tribe.pk

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        role = get_object_or_none(Role, **self.post_data)
        self.assertIsNone(role)

    def test_self(self):
        self.post_data['user'] = self.user.pk

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus409(response)

        role = get_object_or_none(Role, **self.post_data)
        self.assertIsNone(role)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        role = get_object_or_none(Role, **self.post_data)
        self.assertIsNone(role)
