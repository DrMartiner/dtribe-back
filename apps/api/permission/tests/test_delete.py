from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.users.models import User


class DeleteTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:permission:role-detail'

    def setUp(self):
        super(DeleteTest, self).setUp()

        self.tribe: Tribe = G(Tribe, user=self.user)
        self.role: Role = G(Role, tribe=self.tribe)

        self.url = self._get_retrieve_url(self.role)

        self.client.force_login(self.user)

    def test_success(self):
        response = self.client.delete(self.url)
        self.assertResponseStatus204(response)

        role = get_object_or_none(Role, pk=self.role.pk)
        self.assertIsNone(role)

    def test_alien(self):
        role: Role = G(Role)

        url = self._get_retrieve_url(role)
        response = self.client.delete(url)
        self.assertResponseStatus404(response)

        role = get_object_or_none(Role, pk=role.pk)
        self.assertIsNotNone(role)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.delete(self.url)
        self.assertResponseStatus403(response)

        role = get_object_or_none(Role, pk=self.role.pk)
        self.assertIsNotNone(role)
