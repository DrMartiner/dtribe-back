from django.db.models import QuerySet
from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.permission.serializers import RoleListRetrieveSerializer
from apps.permission.models import Role
from apps.tribe.models import Tribe


class ReadListTest(BaseApiTest):
    url = reverse('api_v1:permission:role-list')

    def test_success(self):
        tribe: Tribe = G(Tribe, user=self.user)
        for i in range(3):
            G(Role, tribe=tribe)

        G(Role)

        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        queryset: QuerySet = Role.objects.filter(tribe__user=self.user)
        self.assertListInResponse(RoleListRetrieveSerializer, queryset, response, self.user)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
