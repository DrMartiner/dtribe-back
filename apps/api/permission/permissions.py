from rest_framework.permissions import IsAuthenticated

from apps.permission.models import Role


class RolePermission(IsAuthenticated):
    def has_object_permission(self, request, view, obj: Role):
        return obj.tribe.user == request.user
