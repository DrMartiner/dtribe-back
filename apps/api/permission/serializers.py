from django.utils.translation import gettext_lazy as _

from rest_framework import exceptions
from rest_framework import serializers

from apps.api.exceptions import ConflictError
from .validators import RoleExistsValidator
from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.api.user.serializers import UserListRetrieveSerializer
from apps.permission import models
from apps.tribe.models import Tribe
from apps.users.models import User


class RoleListRetrieveSerializer(serializers.ModelSerializer):
    user = UserListRetrieveSerializer()
    tribe = TribeListRetrieveSerializer()

    class Meta:
        model = models.Role
        fields = '__all__'


class RoleCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True, required=False)

    def validate_user(self, value: User):
        if value.pk == self.context['request'].user.pk:
            raise ConflictError(_('You can\'t add a role for self tribe. You are owner.'))

        return value

    def validate_tribe(self, value: Tribe):
        if value.user.pk != self.context['request'].user.pk:
            raise exceptions.PermissionDenied(_('You are not owner of this Tribe.'))

        return value

    class Meta:
        model = models.Role
        fields = '__all__'
        validators = [RoleExistsValidator()]


class RoleUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Role
        fields = ['role']
