from collections import OrderedDict

from django.utils.translation import gettext_lazy as _

from rest_framework import serializers

from apps.permission.permissions import BaseEntityPermission


class RoleExistsValidator(object):
    def __call__(self, value: OrderedDict):
        is_exists = BaseEntityPermission.is_role_exists(value['user'], value['tribe'], value['role'])
        if is_exists:
            raise serializers.ValidationError(_('The same role is already exists for the user and the tribe.'))
