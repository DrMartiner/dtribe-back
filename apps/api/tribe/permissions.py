from rest_framework.permissions import BasePermission, SAFE_METHODS

from apps.tribe.models import Tribe


class TribePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
           return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: Tribe):
        if request.method in SAFE_METHODS:
            return True
        else:
            return obj.user.pk == request.user.pk
