from django.template.defaultfilters import truncatechars
from rest_framework import serializers

from apps.api.user.serializers import UserListRetrieveSerializer
from apps.subscribe.models import SubscribeUser, SubscribeSession
from apps.tribe.models import Tribe
from apps.users.models import User


class TribeCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    slug = serializers.CharField(read_only=True)

    class Meta:
        model = Tribe
        fields = ['id', 'name', 'slug', 'image', 'description']


class TribeListRetrieveSerializer(serializers.ModelSerializer):
    user = UserListRetrieveSerializer()
    is_my = serializers.SerializerMethodField(read_only=True)
    is_subscribed = serializers.SerializerMethodField(read_only=True)

    def get_is_my(self, obj: Tribe) -> bool:
        user = self._get_user_from_context()
        if user:
            if user.is_authenticated:
                return user == obj.user

        return False

    def get_is_subscribed(self, obj: Tribe) -> bool:
        user = self._get_user_from_context()
        if user and user.is_authenticated:
            is_exists = SubscribeUser.objects.filter(user=user, tribe=obj).exists()
        else:
            if 'request' in self.context:
                session_key = self.context['request'].session.session_key
                is_exists = SubscribeSession.objects.filter(session__session_key=session_key, tribe=obj).exists()
            else:
                is_exists = False

        return is_exists

    def _get_user_from_context(self) -> User:
        if 'request' in self.context:
            return self.context['request'].user

    class Meta:
        model = Tribe
        fields = ['id', 'name', 'slug', 'image', 'description', 'qr_code', 'user', 'is_subscribed', 'is_my']
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class TribeUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tribe
        fields = ['name', 'image', 'description']


class TribeSuggestListSerializer(serializers.ModelSerializer):
    short_description = serializers.SerializerMethodField(read_only=True)
    is_subscribed = serializers.SerializerMethodField(read_only=True)

    def get_short_description(self, obj: Tribe) -> str:
        return truncatechars(obj.description or '', 64)

    def get_is_subscribed(self, obj: Tribe) -> bool:
        user = self._get_user_from_context()
        if user and user.is_authenticated:
            is_exists = SubscribeUser.objects.filter(user=user, tribe=obj).exists()
        else:
            if 'request' in self.context:
                session_key = self.context['request'].session.session_key
                is_exists = SubscribeSession.objects.filter(session__session_key=session_key, tribe=obj).exists()
            else:
                is_exists = False

        return is_exists

    def _get_user_from_context(self) -> User:
        if 'request' in self.context:
            return self.context['request'].user

    class Meta:
        model = Tribe
        fields = ['id', 'name', 'slug', 'image', 'short_description', 'is_subscribed']
