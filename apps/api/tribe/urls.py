from rest_framework.routers import DefaultRouter

from . import views

router = DefaultRouter()
router.register(r'', views.TribeModelViewSet, base_name='tribe')

urlpatterns = router.urls
