from django_filters import FilterSet, NumberFilter, CharFilter, ModelMultipleChoiceFilter

from apps.tribe import models


class TribeFilter(FilterSet):
    user_id = NumberFilter(field_name='user__pk')
    name = CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = models.Tribe
        fields = ['name', 'user_id']
