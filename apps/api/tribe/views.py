from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.tribe.models import Tribe
from apps.api.views import BaseExtendViewSet
from . import serializers
from . import filters
from .permissions import TribePermission


class TribeModelViewSet(BaseExtendViewSet, ModelViewSet):
    lookup_field = 'slug'
    queryset = Tribe.objects.all()
    filter_class = filters.TribeFilter
    permission_classes = [TribePermission]
    serializer_class_map = {
        'create': serializers.TribeCreateSerializer,
        'list': serializers.TribeListRetrieveSerializer,
        'retrieve': serializers.TribeListRetrieveSerializer,
        'update': serializers.TribeUpdateSerializer,
        'partial_update': serializers.TribeUpdateSerializer,
    }

    def perform_create(self, serializer: serializers.TribeCreateSerializer):
        serializer.save(user=self.request.user)

    @action(detail=False, methods=['get'], url_path='recommended', url_name='recommended')
    def recommended_list(self, request, *args, **kwargs):
        queryset = Tribe.objects.all()[:10]
        serializer = serializers.TribeListRetrieveSerializer(queryset, many=True, context={'request': request})

        return Response({'results': serializer.data})

    @action(detail=False, methods=['get'], url_path='suggest', url_name='suggest')
    def suggest_list(self, request, *args, **kwargs):
        name = request.GET.get('name')
        if name:
            queryset = Tribe.objects.filter(name__istartswith=name)[:10]
            serializer = serializers.TribeSuggestListSerializer(queryset, many=True, context={'request': request})
            results = serializer.data
        else:
            results = []

        return Response({'results': results})
