from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.utils import get_object_or_none
from apps.tribe.models import Tribe
from .. import serializers


class DeleteTribeTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:tribe:tribe-detail'

    def setUp(self):
        super(DeleteTribeTest, self).setUp()

        self.instance: Tribe = G(Tribe)
        self.url = self._get_retrieve_url_slug(self.instance)

        self.client.force_login(self.user)

    def test_delete(self):
        self.instance.user = self.user
        self.instance.save()

        response = self.client.delete(self.url)
        self.assertResponseStatus204(response)

        instance = get_object_or_none(Tribe, pk=self.instance.pk)
        self.assertIsNone(instance)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.delete(self.url)
        self.assertResponseStatus403(response)

        instance: Tribe = Tribe.objects.last()
        self.assertIsNotNone(instance)

    def test_alien(self):
        response = self.client.delete(self.url)
        self.assertResponseStatus403(response)

        instance = get_object_or_none(Tribe, pk=self.instance.pk)
        self.assertIsNotNone(instance)
