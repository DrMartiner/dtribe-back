from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.tribe.models import Tribe
from .. import serializers


class UpdateTribeTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:tribe:tribe-detail'

    def setUp(self):
        super(UpdateTribeTest, self).setUp()

        self.tribe: Tribe = G(Tribe)
        self.url = self._get_retrieve_url_slug(self.tribe)

        self.post_data = {
            'slug': 'my-slug',
            'name': 'Tribe new name',
            'image': self.image_file,
            'description': 'Description tribe',
        }

    def test_create(self):
        self.tribe.user = self.user
        self.tribe.save()

        self.client.force_login(self.user)

        response = self.client.patch(self.url, self.post_data, format='multipart')
        self.assertResponseStatus200(response)

        instance: Tribe = Tribe.objects.get(pk=self.tribe.pk)

        self.assertEquals(instance.user.pk, self.user.pk)
        self.assertTrue(bool(instance.image))
        self.assertEquals(instance.name, self.post_data['name'])
        self.assertEquals(instance.description, self.post_data['description'])

        self.assertNotEquals(instance.slug, self.post_data['slug'])

        self.assertRetrieveInResponse(serializers.TribeUpdateSerializer, instance, response)

    def test_un_auth(self):
        response = self.client.post(self.url, self.post_data, format='multipart')
        self.assertResponseStatus403(response)

        self.assertNotUpdated()

    def test_alien(self):
        response = self.client.post(self.url, self.post_data, format='multipart')
        self.assertResponseStatus403(response)

        self.assertNotUpdated()

    def assertNotUpdated(self):
        instance: Tribe = Tribe.objects.get(pk=self.tribe.pk)

        self.assertNotEquals(instance.name, self.post_data['name'])
        self.assertNotEquals(instance.description, self.post_data['description'])

        self.assertNotEquals(instance.slug, self.post_data['slug'])
