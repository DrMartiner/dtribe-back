from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from ...base_test import BaseApiTest
from .. import serializers


class SuggestTribeListTest(BaseApiTest):
    url = reverse('api_v1:tribe:tribe-suggest')
    serializer = serializers.TribeSuggestListSerializer

    def test_un_auth(self):
        for i in range(3):
            G(Tribe, name=f'Big good tribe {i}')
            G(Tribe, name=f'Little good tribe {i}')

        response = self.client.get(self.url, data={'name': 'big'})
        self.assertResponseStatus200(response)

        queryset = Tribe.objects.filter(name__istartswith='big')
        self.assertListInResponse(self.serializer, queryset, response)
