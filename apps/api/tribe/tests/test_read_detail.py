from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from ...base_test import BaseApiTest
from .. import serializers


class ReadDetailTribeTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:tribe:tribe-detail'
    serializer = serializers.TribeListRetrieveSerializer

    def test_by_un_auth(self):
        instance = G(Tribe, slug='slug')

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)
