from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.tribe.models import Tribe
from .. import serializers


class CreateTribeTest(BaseApiTest):
    _url_list = reverse('api_v1:tribe:tribe-list')

    def setUp(self):
        super(CreateTribeTest, self).setUp()

        self.post_data = {
            'slug': 'my-slug',
            'name': 'Tribe new name',
            'image': self.image_file,
            'description': 'Description tribe',
        }

    def test_create(self):
        self.client.force_login(self.user)

        response = self.client.post(self._url_list, self.post_data, format='multipart')
        self.assertResponseStatus201(response)

        instance: Tribe = Tribe.objects.last()
        self.assertIsNotNone(instance)

        self.assertEquals(instance.user.pk, self.user.pk)
        self.assertEquals(instance.name, self.post_data['name'])
        self.assertEquals(instance.description, self.post_data['description'])
        self.assertTrue(bool(instance.image))

        self.assertNotEquals(instance.slug, self.post_data['slug'])

        self.assertRetrieveInResponse(serializers.TribeCreateSerializer, instance, response)

    def test_un_auth(self):
        response = self.client.post(self._url_list, self.post_data, format='multipart')
        self.assertResponseStatus403(response)

        instance: Tribe = Tribe.objects.last()
        self.assertIsNone(instance)
