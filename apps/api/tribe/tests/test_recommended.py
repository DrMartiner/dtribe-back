from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from ...base_test import BaseApiTest
from .. import serializers


class RecommendedTribeListTest(BaseApiTest):
    url = reverse('api_v1:tribe:tribe-recommended')
    serializer = serializers.TribeListRetrieveSerializer

    def test_un_auth(self):
        for i in range(15):
            G(Tribe)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        queryset = Tribe.objects.all()[:10]
        self.assertListInResponse(self.serializer, queryset, response)
