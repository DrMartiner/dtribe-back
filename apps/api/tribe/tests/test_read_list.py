from django.urls import reverse
from django_dynamic_fixture import G

from apps.tribe.models import Tribe
from ...base_test import BaseApiTest
from .. import serializers


class ReadListTribelicTest(BaseApiTest):
    url = reverse('api_v1:tribe:tribe-list')
    serializer = serializers.TribeListRetrieveSerializer

    def test_by_un_auth(self):
        for i in range(3):
            G(Tribe)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, Tribe.objects.all(), response)
