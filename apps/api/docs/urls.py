from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.permissions import AllowAny

schema_view = get_schema_view(
  openapi.Info(
    title='dTrile API',
    default_version='v1',
    description='dTrile API',
    terms_of_service="https://www.google.com/policies/terms/",
    contact=openapi.Contact(email='DrMartiner@GMail.Com'),
    license=openapi.License(name='BSD License'),
  ),
  validators=['flex', 'ssv'],
  public=True,
  permission_classes=(AllowAny, ),
)

urlpatterns = [
  path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]
