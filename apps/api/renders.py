from rest_framework.renderers import JSONRenderer
from django.utils.translation import gettext_lazy as _


class CustomProtocolJSONRenderer(JSONRenderer):
    messages_map = {
        200: _('OK'),
        201: _('Created.'),
        202: _('Accepted.'),
        400: _('Invalid input.'),
        401: _('Incorrect authentication credentials.'),
        403: _('You do not have permission to perform this action.'),
        404: _('Not found.'),
        405: _('Method not allowed.'),
        406: _('Could not satisfy the request Accept header.'),
        415: _('Unsupported media type in request.'),
        500: _('Internal error.'),
    }

    def render(self, data, accepted_media_type=None, renderer_context=None):
        """ Replaced simple JSON data to wrapped json """
        # TODO: check this format in the tests

        if isinstance(data, dict):
            status_code = renderer_context['response'].status_code

            if 200 <= status_code < 300:
                status = 'SUCCESS'
            else:
                status = 'FAIL'

            messages_map = getattr(renderer_context['view'], 'messages_map', {})
            self.messages_map.update(messages_map)
            message = self.messages_map.get(status_code)

            data = {
                'status': status,
                'message': message,
                'payload': data,
            }

        return super(CustomProtocolJSONRenderer, self).render(
            data,
            accepted_media_type,
            renderer_context
        )
