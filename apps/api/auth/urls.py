from django.urls import path

from . import views

urlpatterns = [
    path('sign-in/', views.SignInView.as_view(), name='sign-in'),

    path('sign-up/confirm/', views.SignUpConfirmView.as_view(), name='sign-up-confirm'),
    path('sign-up/', views.SignUpView.as_view(), name='sign-up'),

    path('reset-password/', views.ResetPasswordView.as_view(), name='reset-password'),
]
