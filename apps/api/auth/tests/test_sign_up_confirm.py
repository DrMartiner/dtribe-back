from datetime import timedelta

from django.utils import timezone
from django.conf import settings

from django.urls import reverse

from apps.api.base_test import BaseApiTest


class SignUpEmailConfirmTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up-confirm')

    def test_succeed(self):
        self.update_user_instance(is_active=False)

        response = self.client.get(self.url, data={'code': self.user.sign_up_code})
        self.assertResponseStatus302(response)

        self.assertEquals(response.url, settings.SIGN_UP_CONFIRM_URLS['succeed'])

        self.reload_user_instance()
        self.assertTrue(self.user.is_active)

    def test_expired(self):
        self.update_user_instance(is_active=False)
        created = timezone.now() - timedelta(days=settings.SIGN_UP_CONFIRM_EXPIRED_DAYS + 1)
        self.update_user_instance(sign_up_code_created=created)

        response = self.client.get(self.url, data={'code': self.user.sign_up_code})
        self.assertResponseStatus302(response)

        self.assertEquals(response.url, settings.SIGN_UP_CONFIRM_URLS['expired'])

        self.reload_user_instance()
        self.assertFalse(self.user.is_active)

    def test_activated(self):
        response = self.client.get(self.url, data={'code': self.user.sign_up_code})
        self.assertResponseStatus302(response)

        self.assertEquals(response.url, settings.SIGN_UP_CONFIRM_URLS['activated'])

        self.reload_user_instance()
        self.assertTrue(self.user.is_active)

    def test_not_found(self):
        response = self.client.get(self.url, data={'code': 'unknown'})
        self.assertResponseStatus302(response)

        self.assertEquals(response.url, settings.SIGN_UP_CONFIRM_URLS['unknown'])
