from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.utils import get_user_by_session


class SignInEmailTest(BaseApiTest):
    def setUp(self):
        super(SignInEmailTest, self).setUp()

        self.url = reverse('api_v1:auth:sign-in')
        self.post_data = {
            'email': self.user.email,
            'password': self.password,
        }

    def test_succeed(self):
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.user.pk, user.pk)

    def test_email_uppercase(self):
        self.post_data['email'] = self.post_data['email'].upper()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.user.pk, user.pk)

    def test_wrong_email(self):
        self.post_data['email'] = 'wrong@mail.com'

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        self.assertIsSessionidAnonymous(response)

    def test_wrong_password(self):
        self.post_data['password'] = 'wrong-password'

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        self.assertIsSessionidAnonymous(response)

    def test_wrong_data(self):
        self.post_data['email'] = 'wrong(*&*^%$#@ data'
        del self.post_data['password']

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus400(response)

        self.assertIsSessionidAnonymous(response)
