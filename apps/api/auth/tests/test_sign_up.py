import os
from mock import patch

from django.core import mail
from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.utils import get_user_by_session
from apps.common.utils import get_object_or_none
from apps.users.models import User

PASSWORD = '123'


class SignUpEmailTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up')

    @patch('apps.users.models.User.objects.make_random_password', lambda length=12, allowed_chars='123': PASSWORD)
    def test_succeed(self):
        email = 'new@email.com'
        response = self.client.post(self.url, data={'email': email})
        self.assertResponseStatus201(response)

        user: User = get_object_or_none(User, email__iexact=self.email)
        self.assertIsNotNone(user)
        # self.assertFalse(user.is_active) # TODO: fix True value

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(email, user.email)

        self.assertEquals(email, user.email)

        self.assertEqual(len(mail.outbox), 2)

        self.assertListEqual(mail.outbox[0].to, [email])
        self.assertIn(user.sign_up_code, mail.outbox[0].body)

        self.assertListEqual(mail.outbox[1].to, [email])
        self.assertIn(PASSWORD, mail.outbox[1].body)

    def test_already_exists_not_activated(self):
        response = self.client.post(self.url, {'email': self.user.email})
        self.assertResponseStatus409(response)

        self.assertEqual(len(mail.outbox), 0)

        self.assertIsSessionidAnonymous(response)

    def test_already_exists_activated(self):
        self.update_user_instance(is_active=False)
        
        response = self.client.post(self.url, {'email': self.user.email})
        self.assertResponseStatus409(response)

        self.assertEqual(len(mail.outbox), 1)

        old_sign_up_code = self.user.sign_up_code
        self.reload_user_instance()
        self.assertNotEquals(self.user.sign_up_code, old_sign_up_code)

        self.assertListEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn(self.user.sign_up_code, mail.outbox[0].body)

        self.assertIsSessionidAnonymous(response)

    def test_wrong_data(self):
        response = self.client.post(self.url, {'email': 'wrong(*&*^%$#@email'})
        self.assertResponseStatus400(response)

        self.assertIsSessionidAnonymous(response)
