from mock import patch

from django.core import mail
from django.urls import reverse

from apps.api.base_test import BaseApiTest

PASSWORD = 'new-password-123'


class ResetPasswordTest(BaseApiTest):
    url = reverse('api_v1:auth:reset-password')

    @patch('apps.users.models.User.objects.make_random_password', lambda length=12, allowed_chars='1': PASSWORD)
    def test_succeed(self):
        response = self.client.post(self.url, data={'email': self.user.email})
        self.assertResponseStatus202(response)

        self.assertEqual(len(mail.outbox), 1)

        self.assertListEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn(PASSWORD, mail.outbox[0].body)

    def test_not_active(self):
        self.update_user_instance(is_active=False)

        response = self.client.post(self.url, data={'email': self.user.email})
        self.assertResponseStatus409(response)

        old_sign_up_code = self.user.sign_up_code
        self.reload_user_instance()
        self.assertNotEquals(old_sign_up_code, self.user.sign_up_code)

        self.assertEqual(len(mail.outbox), 1)

        self.assertListEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn(self.user.sign_up_code, mail.outbox[0].body)

        is_right = self.user.check_password(PASSWORD)
        self.assertFalse(is_right)

    def test_not_found(self):
        response = self.client.post(self.url, data={'email': 'unknown@mail.com'})
        self.assertResponseStatus404(response)

        self.assertEqual(len(mail.outbox), 0)
