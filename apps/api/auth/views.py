from django.conf import settings
from django.contrib.auth import login
from django.utils.translation import ugettext_lazy as _
from django.views.generic import RedirectView

from rest_framework import exceptions, status, permissions
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.api.exceptions import ConflictError
from apps.common.utils import get_object_or_none
from apps.users.models import User
from . import serializers


class SignInView(APIView):
    def post(self, request, *args, **kwargs):
        # Add to here others sign-in serializers

        serializer_email = serializers.SignInEmailSerializer(data=request.data)
        is_serializer_email_valid = serializer_email.is_valid(raise_exception=False)

        # Check credentials of others serializers
        if not is_serializer_email_valid:
            raise exceptions.ValidationError(_('Sign-In data is not valid.'))

        status_code = status.HTTP_403_FORBIDDEN

        if is_serializer_email_valid:
            user: User = get_object_or_none(User, email__iexact=serializer_email.data['email'])
            if user:
                if user.is_active:
                    is_right = user.check_password(serializer_email.data['password'])
                    if is_right:
                        login(request, user, 'django.contrib.auth.backends.ModelBackend')
                        status_code = status.HTTP_202_ACCEPTED
                else:
                    status_code = status.HTTP_406_NOT_ACCEPTABLE

        return Response(status=status_code)


class SignUpView(APIView):
    messages_map = {
        202: _('User was created succeed.'),
        409: _('Email already exists.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpEmailSerialzier(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Check exists email
        user: User = get_object_or_none(User, email__iexact=serializer.data['email'])
        if user:
            if not user.is_active:
                user.update_sign_up_code()
                user.send_sign_up_code(request)

            raise ConflictError()

        # Create user
        user = User.objects.create(
            is_active=False,
            email=serializer.data['email'],
            username=serializer.data['email']
        )

        user.update_sign_up_code()
        user.send_sign_up_code(request)

        # Create new password & send it
        password = user.set_new_password()
        user.send_password(request, password)

        # Login the user
        login(request, user, 'django.contrib.auth.backends.ModelBackend')

        return Response(status=status.HTTP_201_CREATED)


class SignUpConfirmView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        code = self.request.GET.get('code')
        user = get_object_or_none(User, sign_up_code=code)
        if not user:
            return settings.SIGN_UP_CONFIRM_URLS['unknown']

        if user.is_active:
            return settings.SIGN_UP_CONFIRM_URLS['activated']

        if user.is_sign_up_code_expired:
            return settings.SIGN_UP_CONFIRM_URLS['expired']

        user.is_active = True
        user.save()

        return settings.SIGN_UP_CONFIRM_URLS['succeed']


class ResetPasswordView(APIView):
    messages_map = {
        202: _('Password was changed'),
        404: _('Email was not found.'),
        409: _('User was not activated.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.RestorePasswordSerialzier(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Check exists email
        user: User = get_object_or_404(User, email__iexact=serializer.data['email'])
        if not user.is_active:
            user.update_sign_up_code()
            user.send_sign_up_code(request)

            raise ConflictError()

        # Create new password & send it
        password = user.set_new_password()
        user.send_password(request, password)

        return Response(status=status.HTTP_202_ACCEPTED)
