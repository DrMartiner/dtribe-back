from rest_framework import serializers


class SignInEmailSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)


class SignUpEmailSerialzier(serializers.Serializer):
    email = serializers.EmailField(required=True)


class RestorePasswordSerialzier(serializers.Serializer):
    email = serializers.EmailField(required=True)
