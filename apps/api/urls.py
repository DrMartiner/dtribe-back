from django.urls import path, include


urlpatterns = [
    path('auth/', include(('apps.api.auth.urls', 'apps.api'), namespace='auth')),
    path('common/', include(('apps.api.common.urls', 'apps.api'), namespace='common')),
    path('docs/', include(('apps.api.docs.urls', 'apps.api'), namespace='docs')),
    path('event/', include(('apps.api.event.urls', 'apps.api'), namespace='event')),
    path('news/', include(('apps.api.news.urls', 'apps.api'), namespace='news')),
    path('permission/', include(('apps.api.permission.urls', 'apps.api'), namespace='permission')),
    path('shop/', include(('apps.api.shop.urls', 'apps.api'), namespace='shop')),
    path('subscribe/', include(('apps.api.subscribe.urls', 'apps.api'), namespace='subscribe')),
    path('tribe/', include(('apps.api.tribe.urls', 'apps.api'), namespace='tribe')),
    path('user/', include(('apps.api.user.urls', 'apps.api'), namespace='user')),
]
