from rest_framework import serializers

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.event.models import Event


class EventListRetrieveSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()

    class Meta:
        model = Event
        fields = [
            'id', 'name', 'slug', 'text', 'location', 'location_short', 'language',
            'date_from', 'time_from', 'date_to', 'time_to', 'tribe',
            'source_link', 'created', 'updated'
        ]
