from django_dynamic_fixture import G

from apps.event.models import Event
from ...base_test import BaseApiTest
from .. import serializers


class ReadDetailEventTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:event:event-detail'
    serializer = serializers.EventListRetrieveSerializer

    def test_un_auth(self):
        instance = G(Event, slug='slug')

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response)
