from django.urls import reverse
from django_dynamic_fixture import G

from apps.event.models import Event
from ...base_test import BaseApiTest
from .. import serializers


class ReadListEventTest(BaseApiTest):
    url = reverse('api_v1:event:event-list')
    serializer = serializers.EventListRetrieveSerializer

    def test_by_un_auth(self):
        for i in range(3):
            G(Event)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, Event.objects.all(), response)
