from django_filters import FilterSet, NumberFilter, CharFilter

from apps.event.models import Event


class EventFilter(FilterSet):
    tribe_id = NumberFilter(field_name='tribe__id')
    tribe_slug = CharFilter(field_name='tribe__slug')
    name = CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Event
        fields = ['name', 'tribe_id', 'tribe_slug']
