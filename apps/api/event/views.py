from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.event.models import Event
from apps.api.views import BaseExtendViewSet
from . import serializers
from . import filters


class EventModelViewSet(BaseExtendViewSet, ReadOnlyModelViewSet):
    lookup_field = 'slug'
    queryset = Event.objects.all()
    filter_class = filters.EventFilter
    serializer_class_map = {
        'list': serializers.EventListRetrieveSerializer,
        'retrieve': serializers.EventListRetrieveSerializer,
    }
