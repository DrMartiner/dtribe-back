from django_filters import FilterSet, NumberFilter, CharFilter, ModelMultipleChoiceFilter

from apps.common.models import Category
from apps.news import models


class NewsFilter(FilterSet):
    tribe_id = NumberFilter(field_name='tribe__id', lookup_expr='exact')
    tribe_slug = CharFilter(field_name='tribe__slug', lookup_expr='exact')
    type = CharFilter(field_name='type', lookup_expr='exact')
    categories = ModelMultipleChoiceFilter(
        field_name='categories',
        to_field_name='id',
        queryset=Category.objects.all(),
    )
    title = CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = models.News
        fields = ['type', 'categories', 'tribe_slug', 'tribe_id']


class SourceFilter(FilterSet):
    news = CharFilter(field_name='news__slug')

    class Meta:
        model = models.Source
        fields = ['news']


class MediaFilter(FilterSet):
    news = CharFilter(field_name='news__slug')
    type = CharFilter(field_name='type', lookup_expr='exact')
    media_status = CharFilter(field_name='media_status', lookup_expr='exact')
    thumbnail_status = CharFilter(field_name='thumbnail_status', lookup_expr='exact')

    class Meta:
        model = models.Media
        fields = ['news', 'type', 'media_status', 'thumbnail_status']
