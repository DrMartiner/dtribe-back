from django.utils.translation import gettext_lazy as _

from rest_framework import exceptions
from rest_framework.permissions import BasePermission, SAFE_METHODS

from apps.news.models import News
from apps.permission.permissions import NewsEntityPermission, MediaNewsEntityPermission


class BaseNewsPermission(BasePermission):
    _permission_class = None

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        else:
           return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj: News):
        permission = self._permission_class(request.user, obj)

        if request.user and not request.user.is_authenticated:
            return request.method in SAFE_METHODS and permission.can_read

        if request.method in SAFE_METHODS:
            return permission.can_read
        else:
            if request.method == 'POST':
                return permission.can_create
            elif request.method == 'PATCH':
                return permission.can_change
            elif request.method == 'DELETE':
                return permission.can_delete
            else:
                response_message = _(f'Method {request.method} is not allowed for this end-point.')
                raise exceptions.MethodNotAllowed(response_message)


class NewsPermission(BaseNewsPermission):
    _permission_class = NewsEntityPermission


class MediaPermission(BaseNewsPermission):
    _permission_class = MediaNewsEntityPermission


class SourcePermission(BaseNewsPermission):
    _permission_class = MediaNewsEntityPermission
