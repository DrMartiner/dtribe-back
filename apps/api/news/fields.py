from rest_framework.relations import PrimaryKeyRelatedField

from apps.news.models import News


class NewsPrimaryKeyRelatedField(PrimaryKeyRelatedField):
    def get_queryset(self):
        return News.active.my(self.context['request'].user)
