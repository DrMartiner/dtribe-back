from django_filters import OrderingFilter
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.api.news import permissions
from apps.news import models
from apps.api.views import BaseExtendViewSet
from apps.permission.permissions import NewsEntityPermission
from . import serializers
from . import filters


class NewsModelViewSet(BaseExtendViewSet, ModelViewSet):
    lookup_field = 'slug'
    filter_class = filters.NewsFilter
    permission_classes = [permissions.NewsPermission]
    serializer_class_map = {
        'create': serializers.NewsCreateSerializer,
        'list': serializers.NewsListRetrieveSerializer,
        'retrieve': serializers.NewsListRetrieveSerializer,
        'update': serializers.NewsUpdateSerializer,
        'partial_update': serializers.NewsUpdateSerializer,
    }

    def get_queryset(self):
        return models.News.active.my(self.request.user).order_by('-pub_date')

    def perform_create(self, serializer):
        if self.request.method == 'POST':
            serializer.save(user=self.request.user)

    @action(detail=True, methods=['post'], url_path='toggle-active', url_name='toggle-active')
    def toggle_favorite(self, request, slug=None):
        obj: models.News = self.get_object()

        permission = NewsEntityPermission(request.user, obj)
        if not permission.can_toggle_active:
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        obj.is_active = not obj.is_active
        obj.save()

        return Response({}, status=status.HTTP_202_ACCEPTED)


class SourceModelViewSet(BaseExtendViewSet, ModelViewSet):
    filter_class = filters.SourceFilter
    permission_classes = [permissions.SourcePermission]
    queryset = models.Source.objects.all()
    serializer_class_map = {
        'create': serializers.SourceCreateSerializer,
        'list': serializers.SourceListRetrieveSerializer,
        'retrieve': serializers.SourceListRetrieveSerializer,
        'update': serializers.SourceUpdateSerializer,
        'partial_update': serializers.SourceUpdateSerializer,
    }

    def get_queryset(self):
        news_slugs = models.News.active.my(self.request.user).values_list('slug', flat=True)
        return self.queryset.model.objects.filter(news__slug__in=news_slugs)


class MediaModelViewSet(BaseExtendViewSet, ModelViewSet):
    filter_class = filters.MediaFilter
    permission_classes = [permissions.MediaPermission]
    queryset = models.Media.objects.all()
    serializer_class_map = {
        'create': serializers.MediaCreateSerializer,
        'list': serializers.MediaListRetrieveSerializer,
        'retrieve': serializers.MediaListRetrieveSerializer,
        'update': serializers.MediaUpdateSerializer,
        'partial_update': serializers.MediaUpdateSerializer,
    }

    def get_queryset(self):
        news_slugs = models.News.active.my(self.request.user).values_list('slug', flat=True)
        return self.queryset.model.objects.filter(news__slug__in=news_slugs)
