from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.news.models import News, Media


class CreateMediaTest(BaseApiTest):
    url = reverse('api_v1:news:media-list')

    def setUp(self):
        super(CreateMediaTest, self).setUp()

        self.news: News = G(News)

        self.post_data = {
            'link': 'https://www.youtube.com/watch?v=pxR3UoO9c9w',
            'title': 'News title',
            'news': self.news.pk,
            'type': Media.TYPE.YOUTUBE_VIDEO,
        }

        self.client.force_login(self.user)

    def test_owner(self):
        self.news.tribe.user = self.user
        self.news.tribe.save()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus201(response)

        media: Media = Media.objects.last()
        self.assertIsNotNone(media)

        self.assertEquals(media.link, self.post_data['link'])
        self.assertEquals(media.title, self.post_data['title'])
        self.assertEquals(media.news.pk, self.post_data['news'])
