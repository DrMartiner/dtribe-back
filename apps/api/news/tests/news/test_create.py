from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.models import Category
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.news.models import News


class CreateNewsTest(BaseApiTest):
    url = reverse('api_v1:news:news-list')

    def setUp(self):
        super(CreateNewsTest, self).setUp()

        self.tribe: Tribe = G(Tribe)

        self.post_data = {
            'title': 'News title',
            'slug': 'my-slug',
            'content': 'News content',
            'is_active': True,
            'type': News.TYPE.TEXT,
            'categories': [
                G(Category, slug='slug1').pk,
                G(Category, slug='slug2').pk,
            ],
            'tribe': self.tribe.pk,
            'pub_date': (timezone.now() + timedelta(days=2)).isoformat()
        }
        G(Category)

        self.client.force_login(self.user)

    def test_owner(self):
        self.tribe.user = self.user
        self.tribe.save()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus201(response)

        news: News = News.objects.last()
        self.assertIsNotNone(news)

        self.assertEquals(news.title, self.post_data['title'])
        self.assertNotEquals(news.slug, self.post_data['slug'])
        self.assertEquals(news.content, self.post_data['content'])
        self.assertFalse(news.is_active)
        self.assertEquals(news.user.pk, self.user.pk)
        self.assertEquals(news.type, self.post_data['type'])
        self.assertEquals(news.tribe.pk, self.tribe.pk)
        self.assertTrue(news.categories.filter(pk__in=self.post_data['categories']).exists())

    def test_moder(self):
        G(Role, tribe=self.tribe, user=self.user, role=Role.ROLE.NEWS_MODER)

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus201(response)

        news: News = News.objects.last()
        self.assertIsNotNone(news)

        self.assertNotEquals(news.slug, self.post_data['slug'])
        self.assertFalse(news.is_active)
        self.assertEquals(news.user.pk, self.user.pk)
        self.assertEquals(news.tribe.pk, self.tribe.pk)

    def test_writer(self):
        G(Role, tribe=self.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus201(response)

        news: News = News.objects.last()
        self.assertIsNotNone(news)

        self.assertNotEquals(news.slug, self.post_data['slug'])
        self.assertFalse(news.is_active)
        self.assertEquals(news.user.pk, self.user.pk)
        self.assertEquals(news.tribe.pk, self.tribe.pk)

    def test_reader(self):
        G(Role, tribe=self.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        news: News = News.objects.last()
        self.assertIsNone(news)

    def test_un_auth(self):
        tribe = G(Tribe)
        self.post_data['tribe'] = tribe.pk

        self.client.logout()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)

        news: News = News.objects.last()
        self.assertIsNone(news)
