from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.models import Category
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.news.models import News


class DeleteNewsTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:news:news-detail'

    def setUp(self):
        super(DeleteNewsTest, self).setUp()

        self.instance: News = G(News)

        self.client.force_login(self.user)

    def test_owner(self):
        self.instance.tribe.user = self.user
        self.instance.tribe.save()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus204(response)

        self.assertDeleted()

    def test_moder(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_MODER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus204(response)

        self.assertDeleted()

    def test_writer(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus403(response)

        self.assertNotDeleted()

    def test_writer_self(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus403(response)

        self.assertNotDeleted()

    def test_reader(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus403(response)

        self.assertNotDeleted()

    def test_reader_self(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus403(response)

        self.assertNotDeleted()

    def test_un_auth(self):
        self.client.logout()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.delete(url)
        self.assertResponseStatus403(response)

        self.assertNotDeleted()

    def assertDeleted(self):
        is_exists = News.objects.filter(pk=self.instance.pk).exists()
        self.assertFalse(is_exists)

    def assertNotDeleted(self):
        is_exists = News.objects.filter(pk=self.instance.pk).exists()
        self.assertTrue(is_exists)
