from _ast import List
from unittest import skip

from django.db.models import Q
from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.news.serializers import NewsListRetrieveSerializer
from apps.common.models import Category
from apps.news import models
from apps.permission.models import Role
from apps.tribe.models import Tribe
from ....base_test import BaseApiTest


class ReadListNewsTest(BaseApiTest):
    url = reverse('api_v1:news:news-list')
    serializer = NewsListRetrieveSerializer

    def setUp(self):
        super(ReadListNewsTest, self).setUp()

        G(models.News)
        for i in range(3):
            G(models.News, is_active=True)

        self.client.force_login(self.user)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, models.News.active.all(), response)

    def test_owner(self):
        tribe = G(Tribe, user=self.user)
        G(models.News, tribe=tribe, is_active=False)
        G(models.News)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        queryset = models.News.objects.filter(Q(tribe__user=self.user) | Q(is_active=True)).order_by('-pub_date')
        self.assertListInResponse(self.serializer, queryset, response, self.user)

    def test_moder(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_MODER)
        G(models.News, tribe=tribe, is_active=False)
        G(models.News)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        tribes_slugs = Role.objects.filter(user=self.user, role__istartswith='news_').values_list('tribe__slug', flat=True)
        queryset = models.News.objects.filter(Q(tribe__slug__in=tribes_slugs) | Q(is_active=True)).order_by('-pub_date')
        self.assertListInResponse(self.serializer, queryset, response, self.user)

    def test_writer(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)
        G(models.News, tribe=tribe, is_active=False)
        G(models.News)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        tribes_slugs = Role.objects.filter(user=self.user, role__istartswith='news_').values_list('tribe__slug', flat=True)
        queryset = models.News.objects.filter(Q(tribe__slug__in=tribes_slugs) | Q(is_active=True)).order_by('-pub_date')
        self.assertListInResponse(self.serializer, queryset, response, self.user)

    def test_reader(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_READER)
        G(models.News, tribe=tribe, is_active=False)
        G(models.News)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        tribes_slugs = Role.objects.filter(user=self.user, role__istartswith='news_').values_list('tribe__slug', flat=True)
        queryset = models.News.objects.filter(Q(tribe__slug__in=tribes_slugs) | Q(is_active=True)).order_by('-pub_date')
        self.assertListInResponse(self.serializer, queryset, response, self.user)

    def test_filtering_by_tribe_slug(self):
        tribe: Tribe = G(Tribe)
        for i in range(3):
            G(models.News, tribe=tribe, is_active=True)
        G(models.News)
        G(models.News, tribe=tribe)

        response = self.client.get(self.url, data={'tribe_slug': tribe.slug})
        self.assertResponseStatus200(response)

        self.assertListInResponse(self.serializer, models.News.active.filter(tribe=tribe), response, self.user)

    @skip('Is not necessary now')
    def test_filtering_by_categories_slugs(self):
        categories: List[Category] = [G(Category), G(Category), G(Category), G(Category)]

        news: models.News = G(models.News)
        news.categories.add(categories[0])
        news.save()

        news: models.News = G(models.News)
        news.categories.add(categories[0])
        news.categories.add(categories[1])
        news.save()

        news: models.News = G(models.News)
        news.categories.add(categories[1])
        news.categories.add(categories[2])
        news.save()

        news: models.News = G(models.News)
        news.categories.add(categories[3])
        news.save()

        categories_slug = [categories[1].slug, categories[2].slug]
        response = self.client.get(self.url, data={'categories': categories_slug})
        self.assertResponseStatus200(response)

        queryset = models.News.active.filter(categories__slug__in=categories_slug)
        self.assertListInResponse(self.serializer, queryset, response, self.user)
