from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.models import Category
from apps.news import models
from apps.permission.models import Role
from apps.tribe.models import Tribe
from ... import serializers


class ReadDetailNewsTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:news:news-detail'
    serializer = serializers.NewsListRetrieveSerializer

    def setUp(self):
        super(ReadDetailNewsTest, self).setUp()

        self.client.force_login(self.user)

    def test_owner(self):
        tribe = G(Tribe, user=self.user)
        instance: models.News = G(models.News, slug='slug', tribe=tribe, is_active=False)

        for i in range(3):
            G(models.Source, news=instance)
            G(models.Media, news=instance)

            category = G(Category)
            instance.categories.add(category)

        instance.save()

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(self.serializer, instance, response, self.user)

    def test_un_auth(self):
        self.client.logout()

        instance: models.News = G(models.News, slug='slug', is_active=False)

        url = self._get_retrieve_url_slug(instance)
        # response = self.client.get(url)
        # self.assertResponseStatus404(response)

        instance.is_active = True
        instance.save()

        response = self.client.get(url)
        self.assertResponseStatus200(response)
        self.assertRetrieveInResponse(self.serializer, instance, response)

    def test_moder(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_MODER)
        instance: models.News = G(models.News, slug='slug', tribe=tribe, is_active=False)

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)
        self.assertRetrieveInResponse(self.serializer, instance, response, self.user)

    def test_writer(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)
        instance: models.News = G(models.News, slug='slug', tribe=tribe, is_active=False)

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)
        self.assertRetrieveInResponse(self.serializer, instance, response, self.user)

    def test_reader(self):
        tribe = G(Tribe)
        G(Role, tribe=tribe, user=self.user, role=Role.ROLE.NEWS_READER)
        instance: models.News = G(models.News, slug='slug', tribe=tribe, is_active=False)

        url = self._get_retrieve_url_slug(instance)
        response = self.client.get(url)
        self.assertResponseStatus200(response)
        self.assertRetrieveInResponse(self.serializer, instance, response, self.user)
