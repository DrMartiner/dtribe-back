from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.models import Category
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.tribe.models import Tribe
from apps.news.models import News


class UpdateNewsTest(BaseApiTest):
    _url_retrieve_named = 'api_v1:news:news-detail'

    def setUp(self):
        super(UpdateNewsTest, self).setUp()

        self.tribe: Tribe = G(Tribe)
        self.post_data = {
            'title': 'News title',
            'slug': 'my-slug',
            'content': 'News content',
            'is_active': True,
            'tribe': self.tribe.pk,
            'type': News.TYPE.VIDEO,
            'categories': [
                G(Category, slug='slug1').pk,
                G(Category, slug='slug2').pk,
            ]
        }

        self.instance: News = G(News)

        self.client.force_login(self.user)

    def test_owner(self):
        self.instance.tribe.user=self.user
        self.instance.tribe.save()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus200(response)

        self.assertUpdated()

    def test_moder(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_MODER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus200(response)

        self.assertUpdated()

    def test_writer(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus403(response)

        self.assertNotUpdated()

    def test_writer_self(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        self.instance.user = self.user
        self.instance.save()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus200(response)

        self.assertUpdated()

    def test_reader(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus403(response)

        self.assertNotUpdated()

    def test_reader_self(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        self.instance.user = self.user
        self.instance.save()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus200(response)

        self.assertUpdated()

    def test_un_auth(self):
        self.client.logout()

        url = self._get_retrieve_url_slug(self.instance)
        response = self.client.patch(url, self.post_data)
        self.assertResponseStatus403(response)

        self.assertNotUpdated()

    def assertUpdated(self):
        updated_news: News = get_object_or_none(News, pk=self.instance.pk)

        self.assertFalse(updated_news.is_active)
        self.assertNotEquals(updated_news.slug, self.post_data['slug'])
        self.assertNotEquals(updated_news.tribe.pk, self.tribe.pk)

        self.assertEquals(updated_news.title, self.post_data['title'])
        self.assertEquals(updated_news.content, self.post_data['content'])
        self.assertEquals(updated_news.type, self.post_data['type'])
        self.assertTrue(updated_news.categories.filter(pk__in=self.post_data['categories']).exists())

    def assertNotUpdated(self):
        updated_news: News = get_object_or_none(News, pk=self.instance.pk)

        self.assertFalse(updated_news.is_active)
        self.assertNotEquals(updated_news.slug, self.post_data['slug'])
        self.assertNotEquals(updated_news.tribe.pk, self.tribe.pk)

        self.assertNotEquals(updated_news.title, self.post_data['title'])
        self.assertNotEquals(updated_news.content, self.post_data['content'])
        self.assertNotEquals(updated_news.type, self.post_data['type'])
        self.assertFalse(updated_news.categories.filter(pk__in=self.post_data['categories']).exists())
