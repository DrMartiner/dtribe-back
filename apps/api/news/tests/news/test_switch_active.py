from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.common.utils import get_object_or_none
from apps.permission.models import Role
from apps.news.models import News


class ToggleActiveTest(BaseApiTest):
    def setUp(self):
        super(ToggleActiveTest, self).setUp()

        self.instance: News = G(News)
        self.url = reverse('api_v1:news:news-toggle-active', kwargs={'slug': self.instance.slug})

        self.client.force_login(self.user)

    def test_owner(self):
        self.instance.tribe.user = self.user
        self.instance.tribe.save()

        response = self.client.post(self.url)
        self.assertResponseStatus202(response)

        self.assertActive(response)

    def test_moder(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_MODER)

        response = self.client.post(self.url)
        self.assertResponseStatus202(response)

        self.assertActive(response)

    def test_writer(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_WRITER)

        response = self.client.post(self.url)
        self.assertResponseStatus403(response)

        self.assertNotActive(response)

    def test_reader(self):
        G(Role, tribe=self.instance.tribe, user=self.user, role=Role.ROLE.NEWS_READER)

        response = self.client.post(self.url)
        self.assertResponseStatus403(response)

        self.assertNotActive(response)

    def test_un_auth(self):
        self.client.logout()

        response = self.client.post(self.url)
        self.assertResponseStatus403(response)

        self.assertNotActive(response)

    def assertActive(self, response):
        news: News = get_object_or_none(News, pk=self.instance.pk)
        self.assertTrue(news.is_active)

    def assertNotActive(self, response):
        news: News = get_object_or_none(News, pk=self.instance.pk)
        self.assertFalse(news.is_active)
