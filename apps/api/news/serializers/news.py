from collections import OrderedDict

from django.utils.translation import gettext_lazy as _

from rest_framework import serializers, exceptions

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.api.common.serializers import CategoryListRetrieveSerializer
from apps.news import models
from apps.permission.permissions import NewsEntityPermission


class NewsCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    slug = serializers.CharField(read_only=True)

    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, attrs['tribe'])
        if not permission.can_create:
            raise exceptions.PermissionDenied(_('You have not permissions for create a news.'))

        return attrs

    class Meta:
        model = models.News
        fields = ['id', 'slug', 'title', 'content', 'type', 'tribe', 'categories', 'pub_date']


class SourceRelateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Source
        fields = ['order', 'title', 'link', 'created', 'updated']


class MediaRelateSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Media
        fields = [
            'order', 'link', 'title', 'type',
            'media_status', 'media_data', 'thumbnail', 'thumbnail_status',
            'created', 'updated',
        ]


class NewsListRetrieveSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()
    categories = CategoryListRetrieveSerializer(many=True)

    media = MediaRelateSerializer(many=True, read_only=True, source='media_set')
    sources = SourceRelateSerializer(many=True, read_only=True, source='source_set')

    class Meta:
        lookup_field = 'slug'
        extra_kwargs = {'url': {'lookup_field': 'slug'}}
        model = models.News
        fields = [
            'id', 'title', 'slug', 'content', 'is_active', 'type', 'tribe', 'categories', 'sources', 'media',
            'pub_date', 'created', 'updated',
        ]


class NewsUpdateSerializer(serializers.ModelSerializer):
    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, self.instance)
        if not permission.can_change:
            raise exceptions.PermissionDenied(_('You have not permissions for change the news.'))

        return attrs

    class Meta:
        lookup_field = 'slug'
        extra_kwargs = {'url': {'lookup_field': 'slug'}}
        model = models.News
        fields = ['title', 'content', 'type', 'categories', 'pub_date']
