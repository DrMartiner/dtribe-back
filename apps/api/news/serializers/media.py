from collections import OrderedDict

from django.utils.translation import gettext_lazy as _

from rest_framework import serializers, exceptions

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.api.common.serializers import CategoryListRetrieveSerializer
from apps.news import models
from apps.permission.permissions import NewsEntityPermission
from ..fields import NewsPrimaryKeyRelatedField


class MediaCreateSerializer(serializers.ModelSerializer):
    news = NewsPrimaryKeyRelatedField()

    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, attrs['news'].tribe)
        if not permission.can_create:
            raise exceptions.PermissionDenied(_('You have not permissions for create a Media for the news.'))

        return attrs

    class Meta:
        model = models.Media
        fields = ['title', 'link', 'news', 'type', 'order']


class NewsRelateSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()
    categories = CategoryListRetrieveSerializer(many=True)

    class Meta:
        model = models.News
        fields = ['title', 'slug', 'is_active', 'user', 'type', 'tribe', 'categories', 'pub_date', 'created', 'updated']


class MediaListRetrieveSerializer(serializers.ModelSerializer):
    news = NewsRelateSerializer()

    class Meta:
        model = models.Media
        fields = [
            'id', 'title', 'link', 'type', 'news',
            'media_status', 'media_data', 'thumbnail', 'thumbnail_status',
            'created', 'updated', 'order',
        ]


class MediaUpdateSerializer(serializers.ModelSerializer):
    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, self.instance.news)
        if not permission.can_change:
            raise exceptions.PermissionDenied(_('You have not permissions for change the Media.'))

        return attrs

    class Meta:
        model = models.Media
        fields = ['title', 'order']
