from collections import OrderedDict

from django.utils.translation import gettext_lazy as _

from rest_framework import serializers, exceptions

from apps.api.tribe.serializers import TribeListRetrieveSerializer
from apps.api.common.serializers import CategoryListRetrieveSerializer
from apps.news import models
from apps.permission.permissions import NewsEntityPermission
from ..fields import NewsPrimaryKeyRelatedField


class SourceCreateSerializer(serializers.ModelSerializer):
    news = NewsPrimaryKeyRelatedField()

    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, attrs['news'].tribe)
        if not permission.can_create:
            raise exceptions.PermissionDenied(_('You have not permissions for create a Source for the news.'))

        return attrs

    class Meta:
        model = models.Source
        fields = ['title', 'link', 'news', 'order']


class NewsRelateSerializer(serializers.ModelSerializer):
    tribe = TribeListRetrieveSerializer()
    categories = CategoryListRetrieveSerializer(many=True)

    class Meta:
        model = models.News
        fields = ['title', 'slug', 'is_active', 'user', 'type', 'tribe', 'categories', 'pub_date', 'created', 'updated']


class SourceListRetrieveSerializer(serializers.ModelSerializer):
    news = NewsRelateSerializer()

    class Meta:
        model = models.Source
        fields = ['id', 'title', 'link', 'news', 'created', 'updated', 'order']


class SourceUpdateSerializer(serializers.ModelSerializer):
    def validate(self, attrs: OrderedDict):
        permission = NewsEntityPermission(self.context['request'].user, self.instance.news)
        if not permission.can_change:
            raise exceptions.PermissionDenied(_('You have not permissions for change the Source.'))

        return attrs

    class Meta:
        model = models.Source
        fields = ['title', 'order']
