from django.http import Http404
from django.test import RequestFactory
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.sessions.models import Session

from apps.api.utils import get_user_by_session
from apps.common.base_test import BaseTest
from apps.common.utils import get_object_or_none
from apps.users.models import User


class BaseApiTest(BaseTest, APITestCase):
    def _get_retrieve_url(self, instance) -> str:
        return reverse(self._url_retrieve_named, args=[instance.id])

    def _get_retrieve_url_slug(self, instance) -> str:
        return reverse(self._url_retrieve_named, args=[instance.slug])

    def assertResponseStatus200(self, response):
        if response.status_code != status.HTTP_200_OK:
            self.fail(f'Status code is not 200. It\'s {response.status_code}')

    def assertResponseStatus201(self, response):
        if response.status_code != status.HTTP_201_CREATED:
            self.fail(f'Status code is not 201. It\'s {response.status_code}')

    def assertResponseStatus202(self, response):
        if response.status_code != status.HTTP_202_ACCEPTED:
            self.fail(f'Status code is not 202. It\'s {response.status_code}')

    def assertResponseStatus204(self, response):
        if response.status_code != status.HTTP_204_NO_CONTENT:
            self.fail(f'Status code is not 204. It\'s {response.status_code}')

    def assertResponseStatus302(self, response):
        if response.status_code != status.HTTP_302_FOUND:
            self.fail(f'Status code is not 302. It\'s {response.status_code}')

    def assertResponseStatus400(self, response):
        if response.status_code != status.HTTP_400_BAD_REQUEST:
            self.fail(f'Status code is not 400. It\'s {response.status_code}')

    def assertResponseStatus401(self, response):
        if response.status_code != status.HTTP_401_UNAUTHORIZED:
            self.fail(f'Status code is not 401. It\'s {response.status_code}')

    def assertResponseStatus403(self, response):
        if response.status_code != status.HTTP_403_FORBIDDEN:
            self.fail(f'Status code is not 403. It\'s {response.status_code}')

    def assertResponseStatus404(self, response):
        if response.status_code != status.HTTP_404_NOT_FOUND:
            self.fail(f'Status code is not 404. It\'s {response.status_code}')

    def assertResponseStatus405(self, response):
        if response.status_code != status.HTTP_405_METHOD_NOT_ALLOWED:
            self.fail(f'Status code is not 405. It\'s {response.status_code}')

    def assertResponseStatus406(self, response):
        if response.status_code != status.HTTP_406_NOT_ACCEPTABLE:
            self.fail(f'Status code is not 406. It\'s {response.status_code}')

    def assertResponseStatus409(self, response):
        if response.status_code != status.HTTP_409_CONFLICT:
            self.fail(f'Status code is not 409. It\'s {response.status_code}')

    def assertResponseStatus410(self, response):
        if response.status_code != status.HTTP_410_GONE:
            self.fail(f'Status code is not 410. It\'s {response.status_code}')

    def assertListInResponse(self, serializer_class, qset, response, context_user: User = None):
        self.assertIn('results', response.data)

        context = self._get_request_context(context_user)
        serializer = serializer_class(many=True, instance=qset, context=context)
        self.assertEquals(len(serializer.data), len(response.data['results']))
        self.assertListEqual(list(serializer.data), response.data['results'])

    def assertRetrieveInResponse(self, serializer_class, instance, response, context_user: User = None):
        context = self._get_request_context(context_user)
        serializer = serializer_class(instance=instance, context=context)
        self.assertDictEqual(serializer.data, response.data)

    def _get_request_context(self, user=None):
        context = {}
        if user:
            request = RequestFactory()
            request.user = user
            context['request'] = request
        return context

    def assertPassword(self, user: User, password: str, will_right=True):
        is_right = user.check_password(password)
        if is_right != will_right:
            if will_right:
                self.fail(f'Password "{password}" is not right for User ID={user.pk}')
            else:
                self.fail(f'Password "{password}" is right User ID={user.pk}')

    def get_request(self, user: User):
        return RequestFactory(user=user)

    def assertSessionid(self, sessionid: str, user_to_compare: User=None) -> None:
        session = get_object_or_none(Session, session_key=sessionid)
        if session is None:
            self.fail(f'Session was not found by sessionid={sessionid}')

        uid = session.get_decoded().get('_auth_user_id')
        user: User = get_object_or_none(User, id=uid)
        if user is None:
            self.fail(f'User was not found by sessionid={sessionid}')

        if user_to_compare is not None:
            if user_to_compare.pk != user.pk:
                self.fail(f'Users ID\'s are not equal: {user.pk} != {user_to_compare.pk}')

    def assertIsSessionidAnonymous(self, response):
        self.assertIn('sessionid', response.client.cookies)
        self.assertRaises(Http404, get_user_by_session, response.client.cookies['sessionid'].value)
