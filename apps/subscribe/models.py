from importlib import import_module

from django.conf import settings
from django.db import models

engine = import_module(settings.SESSION_ENGINE)
session_model = engine.SessionStore.get_model_class()


class SubscribeUser(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'subscribe'
        unique_together = ['user', 'tribe']


class SubscribeSession(models.Model):
    session = models.ForeignKey(session_model, on_delete=models.CASCADE)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'subscribe'
        unique_together = ['session', 'tribe']


# TODO: Create Referer, RefererOne, RefererUser
