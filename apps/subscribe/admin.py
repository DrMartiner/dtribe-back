from django.contrib import admin

from . import models


@admin.register(models.SubscribeUser)
class SubscribeUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'tribe', 'created']
    readonly_fields = ['user', 'tribe', 'created']


@admin.register(models.SubscribeSession)
class SubscribeSessionAdmin(admin.ModelAdmin):
    list_display = ['session', 'tribe', 'created']
    readonly_fields = ['session', 'tribe', 'created']
