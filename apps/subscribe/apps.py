from django.apps import AppConfig


class SubscribeConfig(AppConfig):
    name = 'apps.subscribe'
