import logging
from datetime import timedelta

from django.conf import settings
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.core.exceptions import ValidationError

from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.validators import validate_international_phonenumber
from templated_email import send_templated_mail

logger = logging.getLogger('django.request')


def validate_unique_phone_field(value: str) -> None:
    if not value:
        return

    is_exists = User.objects.filter(phone__exact=value).exists()
    if is_exists:
        raise ValidationError(_('Phone should be unique.'))


class User(AbstractUser):
    USERNAME_FIELD: str = 'email'
    REQUIRED_FIELDS: tuple = ('username', 'phone')

    phone_field_validators = [
        validate_unique_phone_field,
        validate_international_phonenumber,
    ]

    phone = PhoneNumberField(blank=True, null=True, validators=phone_field_validators)
    email = models.EmailField(_('email address'), unique=True)

    sign_up_code = models.CharField(max_length=12, default=get_random_string)
    sign_up_code_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{str(self.phone or "")} {self.email or ""}'

    @property
    def is_sign_up_code_expired(self):
        return timezone.now() > self.sign_up_code_created + timedelta(days=settings.SIGN_UP_CONFIRM_EXPIRED_DAYS)

    def update_sign_up_code(self):
        self.sign_up_code = User.objects.make_random_password(length=12)
        self.sign_up_code_created = timezone.now()
        self.save()

    def send_sign_up_code(self, request) -> None:
        try:
            send_templated_mail(
                template_name='user_sign-up-confirm-code',
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[self.email],
                context={'request': request, 'user': self}
            )
        except Exception as e:
            logger.error(f'Send sign-up code to UserId={self.pk}', exc_info=True)

    def set_new_password(self, password: str=None) -> str:
        if not password:
            password = User.objects.make_random_password()

        self.set_password(password)
        self.save()

        return password

    def send_password(self, request, password: str) -> None:
        # Send the new password
        try:
            send_templated_mail(
                template_name='user_password',
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[self.email],
                context={'request': request, 'password': password}
            )
        except Exception as e:
            logger.error(f'Send password to UserId={self.pk}', exc_info=True)

    @property
    def full_name(self) -> str:
        return self.get_full_name()

    class Meta:
        app_label = 'users'
