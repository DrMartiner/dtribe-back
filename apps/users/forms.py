from django.contrib.auth.forms import (
    UsernameField,
    UserCreationForm as BaseUserCreationForm
)
from .models import User


class UserCreationForm(BaseUserCreationForm):
    class Meta(BaseUserCreationForm.Meta):
        model = User
        fields = ['phone', 'email', 'username']
        field_classes = {'username': UsernameField}
