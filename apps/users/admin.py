from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserCreationForm

from .models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm

    list_display = ['id', 'phone', 'email', 'full_name', 'is_active', 'is_superuser', 'last_login', 'date_joined']
    list_filter = ['last_login', 'date_joined']
    list_editable = ['is_active', ]

    fieldsets = (
        (None, {'fields': ('phone', 'email', 'username', 'password', )}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'email', 'username', 'password1', 'password2'),
        }),
    )
