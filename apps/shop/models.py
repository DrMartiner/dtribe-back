from django.db import models

from apps.common.models import BaseCreatedUpdatedModel


class Item(BaseCreatedUpdatedModel):
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128, unique=True)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='items_images')

    text = models.TextField()

    price = models.FloatField(null=True)
    currency = models.CharField(max_length=4)

    url = models.URLField()

    is_active = models.BooleanField(default=True)

    class Meta:
        app_label = 'shop'
