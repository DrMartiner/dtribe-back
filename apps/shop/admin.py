from django.contrib import admin

from .models import Item


@admin.register(Item)
class ItemAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',), }

    list_display = ['name', 'tribe', 'price', 'currency', 'is_active', 'created', 'updated']
    list_editable = ['is_active']
    list_filter = ['is_active']
    search_fields = ['name', 'text']
    readonly_fields = ['created', 'updated']

    fieldsets = (
        ('Main', {'fields': ('name', 'slug', 'is_active', 'tribe', 'image')}),
        ('Content', {'fields': ('text', 'url', ('price', 'currency'), )}),
        ('Times', {'fields': ('created', 'updated')}),
    )

