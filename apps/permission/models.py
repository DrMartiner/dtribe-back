from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.common.models import BaseCreatedUpdatedModel


class Role(BaseCreatedUpdatedModel):
    class ROLE:
        NEWS_READER = 'news_reader'
        NEWS_WRITER = 'news_writer'
        NEWS_MODER = 'news_moder'

        EVENT_READER = 'event_reader'
        EVENT_WRITER = 'event_writer'
        EVENT_MODER = 'event_moder'

        CHOICES = (
            (NEWS_READER, _('News reader')),
            (NEWS_WRITER, _('News writer')),
            (NEWS_MODER, _('News moder')),

            (EVENT_READER, _('Event reader')),
            (EVENT_WRITER, _('Event writer')),
            (EVENT_MODER, _('Event moder')),
        )

    role = models.CharField(choices=ROLE.CHOICES, max_length=16)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)

    class Meta:
        app_label = 'permission'
        unique_together = ['role', 'user', 'tribe']
