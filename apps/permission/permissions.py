import abc

from apps.tribe.models import Tribe
from apps.news import models as news_models
from apps.users.models import User
from .models import Role


class BaseEntityPermission(abc.ABC):
    _tribe: Tribe = None 
    _user: User = None
    _entity = None

    def __init__(self, user: User, entity) -> None:
        self._user = user
        self._set_tribe(entity)
        
    @abc.abstractmethod
    def _set_tribe(self, entity):
        pass

    @abc.abstractmethod
    def can_create(self) -> bool:
        pass

    @abc.abstractmethod
    def can_change(self) -> bool:
        pass

    @abc.abstractmethod
    def can_delete(self) -> bool:
        pass

    @staticmethod
    def is_role_exists(user: User, tribe: Tribe, role: str) -> bool:
        return Role.objects.filter(user=user, tribe=tribe, role__istartswith=role.split('_')[0]).exists()

    @staticmethod
    def check_permission(user: User, tribe: Tribe, role: str) -> bool:
        return Role.objects.filter(user=user, tribe=tribe, role=role).exists()


class NewsEntityPermission(BaseEntityPermission):
    _entity: news_models.News = None

    def _set_tribe(self, entity):
        if isinstance(entity, Tribe):
            self._tribe = entity
        elif isinstance(entity, news_models.News):
            self._tribe = entity.tribe
            self._entity = entity
        else:
            raise ValueError(f'{type(entity)} is not Tribe or News object')

    @property
    def can_create(self) -> bool:
        is_tribe_owner = self._tribe.user == self._user
        is_moder = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_MODER)
        is_writer = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_WRITER)

        return any([is_tribe_owner, is_moder, is_writer])

    @property
    def can_read(self) -> bool:
        if self._entity.is_active:
            return True
        else:
            is_tribe_owner = self._tribe.user == self._user
            is_moder = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_MODER)
            is_writer = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_WRITER)
            is_reader = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_READER)

            return any([is_tribe_owner, is_moder, is_writer, is_reader])

    @property
    def can_change(self) -> bool:
        is_self = self._entity.user == self._user
        is_tribe_owner = self._tribe.user == self._user
        is_moder = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_MODER)

        return any([is_self, is_tribe_owner, is_moder])

    @property
    def can_delete(self) -> bool:
        is_tribe_owner = self._tribe.user == self._user
        is_moder = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_MODER)

        return any([is_tribe_owner, is_moder])

    @property
    def can_toggle_active(self) -> bool:
        is_tribe_owner = self._tribe.user == self._user
        is_moder = self.check_permission(self._user, self._tribe, Role.ROLE.NEWS_MODER)

        return any([is_tribe_owner, is_moder])


class MediaNewsEntityPermission(NewsEntityPermission):
    _entity: news_models.News = None

    def _set_tribe(self, entity):
        if isinstance(entity, news_models.Media):
            self._tribe = entity.news.tribe
            self._entity = entity.news
        elif isinstance(entity, news_models.News):
            self._tribe = entity.tribe
            self._entity = entity
        else:
            raise ValueError(f'{type(entity)} is not Tribe or Media')
