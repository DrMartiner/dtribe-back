from adminsortable.admin import SortableAdmin
from django.contrib import admin

from . import models


@admin.register(models.Category)
class CategoryAdmin(SortableAdmin):
    prepopulated_fields = {'slug': ('name',), }

    list_display = ['name', ]
    search_fields = ['name']
