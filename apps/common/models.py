from django.db import models
from django.template.defaultfilters import truncatechars
from django.utils.translation import ugettext_lazy as _

from adminsortable.models import Sortable


class BaseCreatedUpdatedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Category(Sortable):
    name = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, unique=True)

    def __str__(self):
        return truncatechars(self.name, 32)

    class Meta:
        app_label = 'common'
        ordering = ['order']
        verbose_name_plural = _('Categories')
