import io

from PIL import Image
from django_dynamic_fixture import G
from django.test.testcases import TestCase

from apps.common.utils import get_object_or_none
from apps.users.models import User


class BaseTest(TestCase):
    email = 'user@test.com'
    password = '123-123-123'
    username = 'username'

    maxDiff = 5000

    def setUp(self):
        super(BaseTest, self).setUp()

        self.user = self.get_user(username=self.username, email=self.email, password=self.password)

    @property
    def image_file(self) -> io.BytesIO:
        file = io.BytesIO()
        image = Image.new('RGBA', size=(1, 1))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        return file

    def get_user(self, password: str = None, **kwargs) -> User:
        user: User = G(User, **kwargs)
        if password:
            user.set_password(password)
        user.save()

        return user

    def reload_user_instance(self) -> None:
        self.user: User = get_object_or_none(User, pk=self.user.pk)

    def update_user_instance(self, **kwargs) -> None:
        self.user.__dict__.update(kwargs)
        self.user.save()
