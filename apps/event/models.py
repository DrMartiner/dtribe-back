from django.conf import settings
from django.db import models
from django.utils import timezone

from apps.common.models import BaseCreatedUpdatedModel


class Event(BaseCreatedUpdatedModel):
    name = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, unique=True)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)
    text = models.TextField(blank=True, null=True)

    location = models.CharField(max_length=256)
    location_short = models.CharField(max_length=256)

    date_from = models.DateField(blank=True, null=True, default=timezone.now)
    time_from = models.TimeField(blank=True, null=True, default=timezone.now)
    date_to = models.DateField(blank=True, null=True, default=timezone.now)
    time_to = models.TimeField(blank=True, null=True, default=timezone.now)

    language = models.CharField(max_length=6, default=settings.LANGUAGE_CODE, choices=settings.LANGUAGES)

    source_link = models.URLField(blank=True, null=True)

    class Meta:
        app_label = 'event'
