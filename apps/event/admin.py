from django.contrib import admin

from .models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',), }

    list_display = ['name', 'tribe', 'location_short', 'date_from', 'date_to', 'time_from', 'time_to', ]
    list_filter = ['date_from', 'date_to']
    search_fields = ['name', 'location']

    fieldsets = (
        ('Main', {'fields': (
            'name', 'slug', 'tribe', 'language', 'location', 'location_short',
            ('date_from', 'date_to'), ('time_from', 'time_to'),
            'source_link', 'text',
        )}),
    )
