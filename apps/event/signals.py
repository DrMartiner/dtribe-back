from random import randint

from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.utils.text import slugify

from .models import Event


@receiver(pre_save, sender=Event)
def set_slug(sender, instance: Event,  *args, **kwargs):
    if instance.pk or instance.slug:
        return

    instance.slug = slugify(f"{randint(11111, 99999)}-{instance.name}")
