import json

import qrcode
from django.core.files.base import ContentFile
from django.core.management import BaseCommand

from apps.tribe.models import Tribe


class Command(BaseCommand):
    def handle(self, *args, **options):
        for tribe in Tribe.objects.all():
            data = json.dumps({'tribe': {'id': tribe.id, 'slug': tribe.slug}})
            qr_file = ContentFile(b'', name=f'{tribe.slug}.png')
            qrcode.make(data).save(qr_file, format='PNG')
            tribe.qr_code = qr_file
            tribe.save()
