from adminsortable.models import Sortable

from django.db import models
from django.template.defaultfilters import truncatechars, slugify
from django.utils.translation import gettext_lazy as _

from apps.common.models import BaseCreatedUpdatedModel


class Tribe(Sortable):
    name = models.CharField(max_length=256)
    slug = models.SlugField(max_length=256, unique=True)

    user = models.ForeignKey('users.User', on_delete=models.SET_NULL, blank=True, null=True)

    image = models.ImageField(upload_to='tribes_images', blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    qr_code = models.ImageField(upload_to='tribes_qr_codes', blank=True, null=True)

    def __str__(self):
        return truncatechars(self.name, 32)

    def make_slug(self) -> str:
        return slugify(self.name)

    class Meta:
        app_label = 'tribe'
        ordering = ['order']


class Referer(BaseCreatedUpdatedModel):
    class TYPE:
        ONE = 'one'
        MANY = 'many'
        PERSONALLY = 'personally'

        CHOICES = (
            (ONE, _('One')),
            (MANY, _('Many')),
            (PERSONALLY, _('Personally')),
        )

    type = models.CharField(max_length=16, choices=TYPE.CHOICES, default=TYPE.MANY)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)
    owner = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='owner')
    for_user = models.ForeignKey('users.User', on_delete=models.CASCADE, blank=True, null=True)
    is_stopped = models.BooleanField(default=False)
    label = models.CharField(max_length=256, blank=True, null=True)

    is_active = models.BooleanField(default=False)

    count_show = models.PositiveIntegerField(default=0)
    count_subscribe = models.PositiveIntegerField(default=0)

    qr_code = models.ImageField(upload_to='referers_qr_codes', blank=True, null=True)

    def __str__(self):
        return str(self.tribe)

    class Meta:
        app_label = 'tribe'
        ordering = ['created']


class Subscribe(BaseCreatedUpdatedModel):
    referer = models.ForeignKey('tribe.Referer', on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, blank=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

    def __str__(self):
        return str(self.referer.tribe)

    class Meta:
        app_label = 'tribe'
        ordering = ['created']


class Show(BaseCreatedUpdatedModel):
    referer = models.ForeignKey('tribe.Referer', on_delete=models.CASCADE)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, blank=True, null=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

    def __str__(self):
        return str(self.referer.tribe)

    class Meta:
        app_label = 'tribe'
        ordering = ['created']
