from django.db import transaction
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from . import tasks
from . import models


@receiver(pre_save, sender=models.Tribe)
def set_news_slug(sender, instance: models.Tribe, *args, **kwargs):
    if not instance.pk:
        instance.slug = instance.make_slug()


@receiver(post_save, sender=models.Tribe)
def generate_qr_code(sender, instance: models.Tribe, created: bool, *args, **kwargs):
    if created:
        with transaction.atomic():
            instance.save()

        transaction.on_commit(
            lambda: tasks.generate_tribe_qr_code.delay(instance.pk)
        )
