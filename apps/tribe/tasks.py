import json
import logging

import qrcode
from django.core.files.base import ContentFile
from celery import shared_task

from apps.common.utils import get_object_or_none
from .models import Tribe

logger = logging.getLogger('django.request')


@shared_task(queue='media')
def generate_tribe_qr_code(instance_id: int):
    tribe: Tribe = get_object_or_none(Tribe, pk=instance_id)
    if not tribe:
        return logger.warning(f'TribeID={instance_id} was not found')

    data = json.dumps({'tribe_slug': tribe.slug})
    qr_file = ContentFile(b'', name=f'{tribe.slug}.png')
    qrcode.make(data).save(qr_file, format='PNG')
    tribe.qr_code = qr_file
    tribe.save()
