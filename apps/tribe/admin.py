from adminsortable.admin import SortableAdmin

from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from . import models


@admin.register(models.Tribe)
class TribeAdmin(SortableAdmin):
    prepopulated_fields = {'slug': ('name',), }

    list_display = ['name', 'qr_code_display']
    search_fields = ['name']
    readonly_fields = ['qr_code']
    fieldsets = (
        (_('Main'), {'fields': ('name', 'slug', 'user')}),
        (_('Description'), {'fields': ('image', 'qr_code', 'description')}),
    )

    def qr_code_display(self, obj: models.Tribe) -> str:
        result = '<div style="height: 100px;"> </div>'
        if bool(obj.qr_code):
            result = f'<img src="{obj.qr_code.url}" height="100" />'
        return mark_safe(result)

    qr_code_display.allow_tags = True
    qr_code_display.short_description = _('QR Code')


class ShowAdminInline(admin.StackedInline):
    extra = 0
    model = models.Show


class SubscribeAdminInline(admin.StackedInline):
    extra = 0
    model = models.Subscribe


@admin.register(models.Referer)
class RefererAdmin(admin.ModelAdmin):
    inlines = [SubscribeAdminInline, ShowAdminInline]
    radio_fields = {'type': admin.HORIZONTAL}

    list_display = [
        'type', 'tribe', 'owner', 'for_user',
        'is_stopped', 'is_active', 'count_show', 'count_subscribe', 'qr_code_display',
        'created', 'updated',
    ]
    list_editable = ['is_stopped']
    search_fields = ['label', 'tribe__name', 'owner__email', 'owner__username']
    readonly_fields = ['qr_code', 'is_active', 'count_show', 'count_subscribe', 'created', 'updated']
    list_filter = ['type', 'is_stopped', 'is_active']
    fieldsets = (
        (_('Main'), {'fields': ('type', 'tribe', 'owner', 'for_user', 'is_stopped', 'label')}),
        (_('Activation'), {'fields': ('is_active', 'count_show', 'count_subscribe', 'qr_code')}),
        (_('Times'), {'fields': ('created', 'updated')}),
    )

    def qr_code_display(self, obj: models.Tribe) -> str:
        result = '<div style="height: 100px;"> </div>'
        if bool(obj.qr_code):
            result = f'<img src="{obj.qr_code.url}" height="100" />'
        return mark_safe(result)

    qr_code_display.allow_tags = True
    qr_code_display.short_description = _('QR Code')

    def save_model(self, request, obj: models.Tribe, form, change):
        if obj.pk:
            pass  # TODO: to generate QR code here

        super(RefererAdmin, self).save_model(request, obj, form, change)
