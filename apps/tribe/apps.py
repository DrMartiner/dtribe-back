from django.apps import AppConfig


class TribeConfig(AppConfig):
    name = 'apps.tribe'

    def ready(self):
        from .signals import generate_qr_code
