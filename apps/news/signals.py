from django.db import transaction
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

from . import tasks
from . import models


@receiver(pre_save, sender=models.News)
def set_news_slug(sender, instance: models.News, *args, **kwargs):
    if not instance.pk:
        instance.slug = instance.make_slug()


@receiver(post_save, sender=models.Media)
def load_media_metadata(sender, instance: models.Media, created: bool, *args, **kwargs):
    if created:
        with transaction.atomic():
            instance.save()

        transaction.on_commit(
            lambda: tasks.load_media_data.delay(instance.pk)
        )
