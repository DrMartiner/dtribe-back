from django.core.management import BaseCommand

from apps.news.models import Media
from apps.news import tasks


class Command(BaseCommand):
    def handle(self, *args, **options):
        for media in Media.objects.all():
            tasks.load_media_data.delay(media.pk)
