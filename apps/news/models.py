from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.template.defaultfilters import truncatechars, slugify

from adminsortable.models import Sortable

from apps.permission.models import Role
from apps.users.models import User
from apps.common.models import BaseCreatedUpdatedModel


class NewsActivatedModelManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True).order_by('-pub_date')

    def my(self, user: User):
        q_objects = Q()

        q_objects.add(Q(is_active=True), Q.OR)

        if user.is_authenticated:
            # Has roles in this tribes
            tribes_slugs = Role.objects.filter(user=user).values_list('tribe__slug', flat=True)
            q_objects.add(Q(tribe__slug__in=tribes_slugs), Q.OR)

            # Is owner
            q_objects.add(Q(tribe__user__id=user.pk), Q.OR)

        return News.objects.filter(q_objects)


class News(BaseCreatedUpdatedModel):
    class TYPE:
        TEXT = 'text'
        AUDIO = 'audio'
        VIDEO = 'video'

        CHOICES = (
            (TEXT, _('Text')),
            (AUDIO, _('Audio')),
            (VIDEO, _('Video')),
        )

    title = models.CharField(max_length=512)
    slug = models.SlugField(max_length=512, unique=True)
    content = models.TextField()
    is_active = models.BooleanField(default=False)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    type = models.CharField(max_length=8, choices=TYPE.CHOICES, default=TYPE.TEXT)
    tribe = models.ForeignKey('tribe.Tribe', on_delete=models.CASCADE)
    categories = models.ManyToManyField('common.Category', blank=True)
    pub_date = models.DateTimeField(default=timezone.now)

    objects = models.Manager()
    active = NewsActivatedModelManager()

    def __str__(self):
        return self.short_title

    class Meta:
        app_label = 'news'
        verbose_name_plural = 'News'
        ordering = ['pub_date']

    @property
    def short_title(self) -> str:
        return truncatechars(self.title, 32)

    def make_slug(self) -> str:
        return slugify(self.title)


class Source(BaseCreatedUpdatedModel, Sortable):
    news = models.ForeignKey('News', on_delete=models.CASCADE)

    title = models.CharField(max_length=256)
    link = models.URLField(max_length=512)

    def __str__(self):
        return f'{truncatechars(self.title, 16)} [{self.news}]'

    class Meta:
        app_label = 'news'
        ordering = ['order']


class Media(BaseCreatedUpdatedModel, Sortable):
    MEDIA_DATA_VERSION = '1'

    class STATUS:
        CREATED = 'created'
        IN_PROGRESS = 'in_progress'
        WAS_FAILED = 'was_failed'
        IS_READY = 'is_ready'

        CHOICES = (
            (CREATED, _('Created')),
            (IN_PROGRESS, _('In progress')),
            (WAS_FAILED, _('Was failed')),
            (IS_READY, _('Is ready'))
        )

    class TYPE:
        VIMEO_AUDIO = 'vimeo_audio'
        VIMEO_VIDEO = 'vimeo_video'
        YOUTUBE_VIDEO = 'youtube_video'
        YOUTUBE_AUDIO = 'youtube_audio'
        APPLE_CAST = 'apple_cast'
        SOUND_CLOUD = 'sound_cloud'

        CHOICES = (
            (VIMEO_AUDIO, 'Vimeo Audio'),
            (VIMEO_VIDEO, 'Vimeo Video'),
            (YOUTUBE_VIDEO, 'YouTube Video'),
            (YOUTUBE_AUDIO, 'YouTube Audio'),
            (APPLE_CAST, 'Apple Cast'),
            (SOUND_CLOUD, 'SoundCloud'),
        )

    news = models.ForeignKey('news.News', on_delete=models.CASCADE)

    link = models.URLField(max_length=512)
    title = models.CharField(max_length=128, blank=True, null=True)
    type = models.CharField(max_length=16, choices=TYPE.CHOICES)

    media_status = models.CharField(max_length=12, choices=STATUS.CHOICES, default=STATUS.CREATED)
    media_data = JSONField(blank=True, null=True)

    thumbnail = models.ImageField(upload_to='media_thumbnails', blank=True, null=True)
    thumbnail_status = models.CharField(max_length=12, choices=STATUS.CHOICES, default=STATUS.CREATED)

    class Meta:
        app_label = 'news'
        ordering = ['order']
