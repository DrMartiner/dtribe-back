from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from adminsortable.admin import SortableAdmin, SortableStackedInline

from . import models


class SourceInlineAdmin(SortableStackedInline):
    extra = 0
    model = models.Source


class MediaInlineAdmin(SortableStackedInline):
    extra = 0
    model = models.Media
    radio_fields = {'type': admin.HORIZONTAL}
    fields = [
        'title', 'type', 'link', 'media_status', 'media_data', 'thumbnail', 'thumbnail_status', 'created', 'updated'
    ]
    readonly_fields = ['media_status', 'media_data', 'thumbnail', 'thumbnail_status', 'created', 'updated']


@admin.register(models.News)
class NewsAdmin(SortableAdmin):
    inlines = [SourceInlineAdmin, MediaInlineAdmin]

    filter_horizontal = ['categories']
    prepopulated_fields = {'slug': ('title',), }
    radio_fields = {'type': admin.HORIZONTAL}

    list_display = ['short_title', 'type', 'tribe', 'is_active', 'user', 'pub_date', 'created', 'updated']
    list_filter = ['type', 'is_active', 'created', 'updated']
    search_fields = ['title', 'content']
    readonly_fields = ['created', 'updated']

    fieldsets = (
        (_('Main'), {'fields': ('title', 'slug', 'type', 'tribe', 'user', 'is_active', 'pub_date')}),
        (_('Categories'), {'fields': ('categories',)}),
        (_('Content'), {'fields': ('content',)}),
        (_('Timestamps'), {'fields': ('created', 'updated')}),
    )
