import re

import requests

from apps.news import models
from .base import BaseMediaLoader


class SoundCloudLoader(BaseMediaLoader):
    _CLIENT_ID = 'yKErPEAPC9QCeJnXv3FNzzRKaEqRZua6'
    _page_content: str = None
    _track_id: int = None

    _LINK_PATTERN = r'(https?://)?(www\.)?soundcloud.com/(.*)'

    def __init__(self, *args, **kwargs):
        super(SoundCloudLoader, self).__init__(*args, **kwargs)

        self._load_page_content()
        self._parse_track_id()

    def _load_page_content(self):
        error_message = f'Loading SoundCloud ID={self.instance.pk} page was fail'
        try:
            response = requests.get(self.instance.link)
            if response.ok:
                self._page_content = response.text
            else:
                self._log('warning', error_message, True, Exception(error_message))
        except Exception as e:
            self._log('warning', error_message, True, e)

    def _parse_track_id(self):
        m = re.search(r'soundcloud://sounds:(.+?)"', self._page_content)
        if not m:
            message = f'soundcloud://sounds: was not found at SoundCloud ID={self.instance.pk}'
            self._log('warning', message, False, ValueError(message))

        self._track_id = m.group(1)

    def _load_media(self) -> None:
        self.instance.media_data = {
            'version': models.Media.MEDIA_DATA_VERSION,
            'audio': {
                'url': f'https://api.soundcloud.com/tracks/{self._track_id}/stream?client_id={self._CLIENT_ID}',
                'quality': '128',
                'extension': 'mp3',
            }
        }

        # Loading duration for particular end-point
        url = f'https://api.soundcloud.com/tracks/{self._track_id}?client_id={self._CLIENT_ID}'
        response = requests.get(url)
        if response.ok:
            self.instance.media_data['duration'] = round(response.json()['duration'] / 1000)
        else:
            message = f'Loading duration for SoundCloud ID={self.instance.pk} was failure'
            self._log('warning', message, False)

        self.instance.save()

    def _get_thumbnail_url(self) -> str:
        # Loading duration for particular end-point
        url = f'https://api.soundcloud.com/tracks/{self._track_id}?client_id={self._CLIENT_ID}'
        response = requests.get(url)
        if response.ok:
            return response.json()['artwork_url']
