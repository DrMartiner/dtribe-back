import re
import logging
from abc import ABC, abstractmethod

import requests
from io import BytesIO

from django.core import files

from apps.news import models


class BaseMediaLoader(ABC):
    _instance = None
    _instance_id: int = None

    _LINK_PATTERN: str = None

    _logger = logging.getLogger('media_loader')

    def __init__(self, instance_id: int) -> None:
        self._instance_id = instance_id

        self._check_link()

    def _check_link(self) -> None:
        m = re.match(self._LINK_PATTERN, self.instance.link)
        if not m:
            message = f'MediaID={self.instance.pk} URL={self.instance.link} is not mach'
            self._log('warning', message, False, ValueError(message))

    def load(self) -> None:
        self.load_media()
        self.load_thumbnail()

    def load_media(self) -> None:
        self._set_media_status(models.Media.STATUS.IN_PROGRESS)

        try:
            self._load_media()
        except Exception as e:
            self._set_media_status(models.Media.STATUS.WAS_FAILED)

            message = f'MediaID={self.instance.pk} was failure'
            self._log('error', message, True, e)

        self._set_media_status(models.Media.STATUS.IS_READY)

    def load_thumbnail(self) -> None:
        self._set_thumbnail_status(models.Media.STATUS.IN_PROGRESS)

        try:
            self._load_thumbnail()
        except Exception as e:
            self._set_thumbnail_status(models.Media.STATUS.WAS_FAILED)

            message = f'Loading thumbnail for MediaID={self.instance.pk} was failure'
            self._log('error', message, True, e)

        self._set_thumbnail_status(models.Media.STATUS.IS_READY)

    @property
    def instance(self):
        if not self._instance:
            try:
                self._instance = models.Media.objects.get(pk=self._instance_id)
            except models.Media.DoesNotExist as e:
                message = f'MediaID={self._instance_id} was not found'
                self._log('error', message, False, e)
            except Exception as e:
                message = f'Getting MediaID={self._instance_id} was failure'
                self._log('error', message, False, e)

        return self._instance

    @abstractmethod
    def _load_media(self) -> None:
        pass

    def _load_thumbnail(self) -> None:
        thumbnail_url = self._get_thumbnail_url()
        if not thumbnail_url:
            return

        response = requests.get(thumbnail_url)
        if response.status_code == requests.codes.ok:
            fp = BytesIO()
            fp.write(response.content)

            filename = f'{self.instance.news.id}-{self.instance.news.slug}.jpg'
            self.instance.thumbnail.save(filename, files.File(fp))
            self.instance.save()
        else:
            message = f'MediaID={self.instance.pk} HTTP status={response.status_code} of download {thumbnail_url}'
            self._log('warning', message, False)

    @abstractmethod
    def _get_thumbnail_url(self) -> str:
        pass

    def _set_media_status(self, status: str) -> None:
        try:
            self.instance.media_status = status
            self.instance.save(update_fields=['media_status'])
        except Exception as e:
            message = f'Setting media_status "{status}" for MediaID={self.instance.pk} was failure'
            self._log('error', message, True, e)

    def _set_thumbnail_status(self, status: str) -> None:
        try:
            self.instance.thumbnail_status = status
            self.instance.save(update_fields=['thumbnail_status'])
        except Exception as e:
            message = f'Setting thumbnail_status "{status}" for MediaID={self.instance.pk} was failure'
            self._log('error', message, True, e)

    def _log(self, action: str, message: str, exc_info=True, e: Exception=None) -> None:
        getattr(self._logger, action)(message, exc_info=exc_info)

        if e:
            raise e
