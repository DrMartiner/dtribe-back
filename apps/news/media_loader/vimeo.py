import re

import requests
from pytimeparse.timeparse import timeparse
import vimeo_dl as vimeo
from vimeo_dl.vimeo_internal import InternStream
from vimeo_dl.compat import (
    compat_request,
    compat_urlerr,
    compat_httperr,
    std_headers,
    json_read,
    compat_urlopen,
)

from .base import BaseMediaLoader
from apps.news import models


def _extract_json(url):
    json_info = {}
    try:
        req_json = compat_request(url, headers=std_headers)
        resp_json = compat_urlopen(req_json)

    except (compat_urlerr, compat_httperr) as e:
        return e
    else:
        jdata = resp_json.read().decode('utf-8')
        json = json_read(jdata)
        json_info = {
            "title": json[0]['title'],
            "user": json[0]['user_name'],
            "privacy": json[0]['embed_privacy'],
            "height": json[0]['height'],
            "width": json[0]['width'],
            "upload_date": json[0]['upload_date'],
            "likes": 0,
            "comments": json[0]['stats_number_of_comments'],
            "user_url": json[0]['user_url'],
            "video_id": json[0]['id'],
            "duration": json[0]['duration'],
            "user_id": json[0]['user_id'],
            "url": json[0]['url'],
            "viewed": json[0]['stats_number_of_plays'],
        }
        return json_info


from vimeo_dl import extractor
extractor._extract_json = _extract_json


class BaseVimeoLoader(BaseMediaLoader):
    model_class = None

    _LINK_PATTERN = r'(https?://)?(www\.)?vimeo\.([a-z]{2,4})/(\d+)'

    _IS_LOADING_AUDIO = True
    _IS_LOADING_VIDEO = True

    def __init__(self, *args, **kwargs):
        super(BaseVimeoLoader, self).__init__(*args, **kwargs)

        if not any([self._IS_LOADING_AUDIO, self._IS_LOADING_VIDEO]):
            message = f'Should has _IS_LOADING_AUDIO or _IS_LOADING_VIDEO as True'
            self._log('warning', message, False, AttributeError(message))

    def _load_media(self) -> None:
        source = self._get_source()

        self.instance.media_data = {
            'version': models.Media.MEDIA_DATA_VERSION,
            'duration': timeparse(source._parent.duration),
        }

        if self._IS_LOADING_AUDIO:
            self.instance.media_data['audio'] = []
        if self._IS_LOADING_VIDEO:
            video = vimeo.new(self.instance.link)
            self.instance.media_data['video'] = [
                {'url': s.url, 'quality': s.quality, 'extension': s.extension} for s in video.streams
            ]

        self.instance.save()

    def _get_audio_stream_data(self, stream: InternStream) -> dict:
        raise NotImplementedError()

    def _get_video_stream_data(self, stream: InternStream) -> dict:
        return {
            'url': stream.url,
            'quality': stream.quality,
            'extension': stream.extension,
        }

    def _get_thumbnail_url(self) -> str:
        m = re.match(self._LINK_PATTERN, self.instance.link)
        url = f'http://vimeo.com/api/v2/video/{m.groups()[-1]}.json'

        response = requests.get(url)
        if response.ok:
            return response.json()[0]['thumbnail_large']

    def _get_source(self) -> InternStream:
        video = vimeo.new(self.instance.link)

        if not video.streams:
            message = f'There was not streams from URL={self.instance.link}'
            self._log('warning', message, False, ValueError(message))

        return video.streams[0]


class VimeoVideoLoader(BaseVimeoLoader):
    _IS_LOADING_AUDIO = False
    _IS_LOADING_VIDEO = True

