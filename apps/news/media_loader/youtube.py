import pafy
from pafy.backend_youtube_dl import YtdlPafy, YtdlStream

from .base import BaseMediaLoader
from apps.news import models


class BaseYouTubeLoader(BaseMediaLoader):
    _LINK_PATTERN = r'(https?://)?(www\.)?youtube\.([a-z]{2,4})/watch\?v=([-\w]+)'

    _IS_LOADING_AUDIO = True
    _IS_LOADING_VIDEO = True

    def __init__(self, *args, **kwargs):
        super(BaseYouTubeLoader, self).__init__(*args, **kwargs)

        if not any([self._IS_LOADING_AUDIO, self._IS_LOADING_VIDEO]):
            message = f'should has _IS_LOADING_AUDIO or _IS_LOADING_VIDEO as True'
            self._log('warning', message, False, AttributeError(message))

    def _load_media(self) -> None:
        source = self._get_source()

        self.instance.media_data = {
            'version': models.Media.MEDIA_DATA_VERSION,
            'duration': source.length,
        }

        if self._IS_LOADING_AUDIO:
            self.instance.media_data['audio'] = self.instance.media_data['video'] = [
                {'url': s.url, 'quality': s.quality, 'extension': s.extension} for s in source.audiostreams
            ]
        if self._IS_LOADING_VIDEO:
            self.instance.media_data['video'] = [
                {'url': s.url, 'quality': s.quality, 'extension': s.extension} for s in source.videostreams
            ]

        self.instance.save()

    def _get_audio_stream_data(self, stream: YtdlStream) -> dict:
        return {
            'url': stream.url,
            'quality': stream.quality,
            'extension': stream.extension,
        }

    def _get_video_stream_data(self, stream: YtdlStream) -> dict:
        return {
            'url': stream.url,
            'quality': stream.quality,
            'extension': stream.extension,
        }

    def _get_thumbnail_url(self) -> str:
        return self._get_source().bigthumbhd

    def _get_source(self) -> YtdlPafy:
        source: YtdlPafy = pafy.new(self.instance.link, gdata=True)

        if not source.streams:
            message = f'There was not streams from URL={self.instance.link}'
            self._log('warning', message, False, ValueError(message))

        return source


class YouTubeVideoLoader(BaseYouTubeLoader):
    _IS_LOADING_AUDIO = True
    _IS_LOADING_VIDEO = True


class YouTubeAudioLoader(BaseYouTubeLoader):
    _IS_LOADING_AUDIO = True
    _IS_LOADING_VIDEO = False
