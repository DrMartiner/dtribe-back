from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'apps.news'

    def ready(self):
        from .signals import set_news_slug, load_media_metadata
