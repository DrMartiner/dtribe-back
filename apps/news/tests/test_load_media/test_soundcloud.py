from .base import BaseMediaLoaderTest
from apps.news import media_loader


class YouTubeAudioTest(BaseMediaLoaderTest.TestCase):
    loader_class = media_loader.SoundCloudLoader

    _instance_creating_args = {
        'link': 'https://soundcloud.com/jun-kai-9/sidharma-complex',
    }
