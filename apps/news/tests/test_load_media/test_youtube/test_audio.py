from .base import BaseYouTubeTest
from ..base import BaseMediaLoaderTest
from apps.news import media_loader


class YouTubeAudioTest(BaseYouTubeTest, BaseMediaLoaderTest.TestCase):
    loader_class = media_loader.YouTubeAudioLoader

    def test_load_media(self):
        super(YouTubeAudioTest, self).test_load_media()

        self.assertIn('audio', self.instance.media_data)
        self.assertNotIn('video', self.instance.media_data)
