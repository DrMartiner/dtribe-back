from .base import BaseYouTubeTest
from ..base import BaseMediaLoaderTest
from apps.news import media_loader


class YouTubeVideoTest(BaseYouTubeTest, BaseMediaLoaderTest.TestCase):
    loader_class = media_loader.YouTubeVideoLoader

    def test_load_media(self):
        super(YouTubeVideoTest, self).test_load_media()

        self.assertIn('audio', self.instance.media_data)
        self.assertIn('video', self.instance.media_data)
