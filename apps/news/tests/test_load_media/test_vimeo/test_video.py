from .base import BaseVimeoTest
from ..base import BaseMediaLoaderTest
from apps.news import media_loader


class VimeoVideoTest(BaseVimeoTest, BaseMediaLoaderTest.TestCase):
    loader_class = media_loader.VimeoVideoLoader

    def test_load_media(self):
        super(VimeoVideoTest, self).test_load_media()

        self.assertNotIn('audio', self.instance.media_data)

        self.assertIn('video', self.instance.media_data)
        self.assertIsNotNone(self.instance.media_data['video'])
