from django_dynamic_fixture import G

from apps.common.base_test import BaseTest
from apps.news.models import Media


class BaseMediaLoaderTest:
    class TestCase(BaseTest):
        instance = None

        loader_class = None

        _instance_creating_args = {}

        # @skip('To mock loader.load_media')
        def test_load_media(self):
            self._create_instance()

            loader = self.loader_class(self.instance.pk)
            loader.load_media()

            self._reload_instance()

            self.assertEquals(self.instance.media_status, Media.STATUS.IS_READY)
            self.assertIsNotNone(self.instance.media_data)

        # @skip('To mock loader.load_thumbnail')
        def test_load_thumbnail(self):
            self._create_instance(thumbnail=None)

            loader = self.loader_class(
                self.instance.pk
            )
            loader.load_thumbnail()

            self._reload_instance()

            self.assertEquals(self.instance.thumbnail_status, Media.STATUS.IS_READY)
            self.assertTrue(bool(self.instance.thumbnail))

        def _create_instance(self, **kwargs):
            self._instance_creating_args.update(kwargs)

            self.instance = G(Media, **self._instance_creating_args)

        def _reload_instance(self) -> None:
            self.instance = Media.objects.get(pk=self.instance.pk)
