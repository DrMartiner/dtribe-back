from django.test import TestCase
from django_dynamic_fixture import G

from apps.news.models import News


class NewsSlugTest(TestCase):
    def test_make_slug(self):
        news: News = G(News, title='Long great title (*^&%$#@',)

        created_slug = news.make_slug()
        self.assertEquals(news.slug, created_slug)
