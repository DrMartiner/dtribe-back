import logging

from celery import shared_task

from apps.common.utils import get_object_or_none
from apps.news.models import Media
from . import media_loader

logger = logging.getLogger('django.request')


def _run_loader(media: Media):
    if media.type == Media.TYPE.VIMEO_VIDEO:
        loader_class = media_loader.VimeoVideoLoader
    elif media.type == Media.TYPE.YOUTUBE_AUDIO:
        loader_class = media_loader.YouTubeAudioLoader
    elif media.type == Media.TYPE.YOUTUBE_VIDEO:
        loader_class = media_loader.YouTubeVideoLoader
    elif media.type == Media.TYPE.SOUND_CLOUD:
        loader_class = media_loader.SoundCloudLoader
    else:
        return

    loader = loader_class(media.pk)
    loader.load()


@shared_task(queue='media')
def load_media_data(instance_id: int):
    media: Media = get_object_or_none(Media, pk=instance_id)
    if not media:
        return logger.warning(f'MediaID={instance_id} was not found')
    _run_loader(media)
