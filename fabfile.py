from fabric2 import Connection, task


@task
def up(c):
    connect = Connection('dtribe_stage')
    excludes = [
        '.git*',
        'tmp',
        'Pipfile.lock',
        '__pycache__',
        '.env',
        'cache/*',
        'media/*',
        'static/*',
        'project/settings/local.py',
        '',
        '',
    ]
    excl = ' '.join([f"--exclude='{e}'" for e in excludes])
    connect.local(f"rsync -av -e ssh {excl} * dtribe_stage:/opt/dtribe/back/")

