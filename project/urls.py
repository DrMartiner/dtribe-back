from django.conf import settings
from django.contrib import admin
from django.urls import (path, include)

urlpatterns = [
    path('api/v1/', include(('apps.api.urls', 'apps.api'), namespace='api_v1')),

    path('admin/django/', admin.site.urls),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(prefix=settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
