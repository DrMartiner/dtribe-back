import json
import logging
import time
from importlib import import_module

from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
from django.contrib.sessions.middleware import SessionMiddleware as DjangoSessionMiddleware

engine = import_module(settings.SESSION_ENGINE)
SessionStore = engine.SessionStore

logger = logging.getLogger('middleware_request')


class SessionMiddleware(DjangoSessionMiddleware):
    # TODO: Add CSRF token the same
    pass

    def process_request(self, request):
        request.csrf_exempt = True

        has_session = settings.SESSION_COOKIE_NAME in request.COOKIES

        session_key = request.META.get('HTTP_X_SESSIONID')
        if session_key and not has_session:
            has_session = True
            request.COOKIES[settings.SESSION_COOKIE_NAME] = session_key

        if has_session:
            super(SessionMiddleware, self).process_request(request)
        else:
            session = engine.SessionStore()
            session.create()

            request.session = session

            request.COOKIES[settings.SESSION_COOKIE_NAME] = session.session_key

    def process_response(self, request, response):
        response = super(SessionMiddleware, self).process_response(request, response)
        if request.session.session_key:
            response['X-SessionID'] = request.session.session_key
        return response


class RequestLogMiddleware(MiddlewareMixin):
    # TODO: add to settings
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process_request(self, request):
        request.request_body = request.body
        if request.method in ['POST', 'PUT', 'PATCH']:
            if request.content_type == 'application/json':
                try:
                    request.request_body = json.loads(request.body)
                except:
                    pass
            else:
                request.request_body = request.body[:200]

        request._start_time = time.time()

    def process_response(self, request, response):
        if '/api/' not in request.get_full_path():
            return response

        payload = {}
        if response.get('content-type') == 'application/json':
            if getattr(response, 'streaming', True):
                payload = json.loads(response.content)

        cookies = {str(name): str(request.COOKIES[name]) for name in request.COOKIES.keys()}

        uid = ''
        if request.user.is_authenticated:
            uid = request.user.pk

        sid = ''
        if request.session.session_key:
            sid = request.session.session_key

        log_data = {
            'uid': uid,
            'sid': sid,

            'remote_address': self.get_client_ip(request),
            'content_type': response.get('content-type', ''),

            'method': request.method,
            'status': response.status_code,
            'path': request.get_full_path(),
            'payload': payload,
            'request': request.request_body,
            'cookies': cookies,

            'run_time': time.time() - request._start_time,
        }
        logger.info('', extra=log_data)

        return response

    def get_client_ip(self, request) -> str:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
