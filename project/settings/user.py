from .env import env

AUTH_USER_MODEL = 'users.User'

PHONENUMBER_DB_FORMAT = 'INTERNATIONAL'
PHONENUMBER_DEFAULT_REGION = env('PHONENUMBER_DEFAULT_REGION', default='+7')

SESSION_SAVE_EVERY_REQUEST = True

SIGN_UP_CONFIRM_EXPIRED_DAYS = 5

SIGN_UP_CONFIRM_URLS = {
    'succeed': '/auth/sign-up/succeed',
    'expired': '/auth/sign-up/expired',
    'activated': '/auth/sign-up/activated',
    'unknown': '/auth/sign-up/key-not-found',
}
