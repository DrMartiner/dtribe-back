import os
from logging import config

from django.utils.translation import gettext_lazy as _

from .env import env, root

os.sys.path.insert(0, root())
os.sys.path.insert(0, os.path.join(root(), 'apps'))

ROLE = env('ROLE')

DEBUG = env('DEBUG')

SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = env('ALLOWED_HOSTS')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('POSTGRES_DB'),
        'USER': os.environ.get('POSTGRES_USER'),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD'),
        'HOST': os.environ.get('POSTGRES_HOST'),
    }
}

FIXTURE_DIRS = [
    root('project/fixtures'),
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',

    'adminsortable',
    'corsheaders',
    'djcelery_email',
    'django_filters',
    'drf_yasg',
    'flex',
    'flower',
    'phonenumber_field',
    'rest_framework',

    'apps.common.apps.CommonConfig',
    'apps.event.apps.EventConfig',
    'apps.tribe.apps.TribeConfig',
    'apps.news.apps.NewsConfig',
    'apps.permission.apps.PermissionConfig',
    'apps.shop.apps.ShopConfig',
    'apps.subscribe.apps.SubscribeConfig',
    'apps.users.apps.UsersConfig',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'project.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'project.middleware.RequestLogMiddleware',
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            root('project/templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

USE_I18N = True
USE_L10N = True

SITE_ID = 1

LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('en', _('English')),
    ('ru', _('Russian')),
]

USE_TZ = True
TIME_ZONE = 'Europe/Moscow'

MEDIA_URL = '/media/'
MEDIA_ROOT = root('media')

STATIC_URL = '/static/'
STATIC_ROOT = root('static')

STATICFILES_DIRS = [
    root('project/static')
]

BASE_URL = env('BASE_URL')

EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'dTribe No-Reply <no-reply@mail.dtribe.one>'

TEMPLATED_EMAIL_FILE_EXTENSION = 'html'

EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'

# Load loggers
logger_config_path = root('.loggers.ini')
if not os.path.exists(logger_config_path):
    logger_config_path = root(f'project/loggers/{ROLE}.ini')

config.fileConfig(logger_config_path)

