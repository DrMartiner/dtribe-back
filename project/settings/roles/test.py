from random import randint

DEBUG = False


class DisableMigrations(object):
    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return None


MIGRATION_MODULES = DisableMigrations()

AUTH_PASSWORD_VALIDATORS = []
PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher', ]


DDF_FIELD_FIXTURES = {
    'phonenumber_field.modelfields.PhoneNumberField': {'ddf_fixture': lambda: f'+7999{randint(1111111, 9999999)}'},
}

CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
BROKER_BACKEND = 'memory'
