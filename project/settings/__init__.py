import os

from split_settings.tools import include, optional

ROLE = os.environ.get('ROLE')

# Parts of settings
include(
    'common.py',
    'celery.py',
    'rest_framework.py',
    'request.py',
    'user.py',
    f'roles/{ROLE}.py',
    optional('local.py')  # Local settings
)
