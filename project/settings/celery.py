from .env import env
from . import common

BROKER_URL = env('BROKER_URL')
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = env('CELERY_RESULT_BACKEND_URL')
CELERY_ACCEPT_CONTENT = ['application/x-python-serialize']
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'json'

CELERY_TIMEZONE = common.TIME_ZONE
CELERY_ENABLE_UTC = True

CELERY_APP = 'project'

CELERYD_LOG_LEVEL = 'INFO'
CELERYD_PID_FILE = '/tmp/celery-%I.pid'
CELERYD_OPTS = "--concurrency=8"

CELERYBEAT_OPTS = '--schedule="/tmp/celerybeat-schedule-%I"'
