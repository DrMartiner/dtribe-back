import os

import environ

root = environ.Path(__file__, '../../..')
env = environ.Env(
    DEBUG=(bool, False),
    BASE_URL=(str, None),
    ALLOWED_HOSTS=(list, ['*']),
    SECRET_KEY=(str, 'r4n*gbglj5dsdaddf$2y-4lg$(u^0w32uyocv6uk'),
    BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_RESULT_BACKEND_URL=(str, 'redis://redis:6379'),

    EMAIL_HOST_USER=(str, None),
    EMAIL_HOST_PASSWORD=(str, None),
)

# Load env.ini
env_file_path = root('.env.ini')
if not os.path.exists(env_file_path):
    env_file_path = root('env.ini')
env.read_env(env_file_path)
